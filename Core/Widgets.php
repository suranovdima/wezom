<?php
namespace Core;

use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Items;
use Modules\News\Models\News;
use Core\QB\DB;
use Modules\Cart\Models\Cart;
use Modules\Catalog\Models\Filter;

/**
 *  Class that helps with widgets on the site
 */
class Widgets
{

    static $_instance; // Constant that consists self class

    public $_data = []; // Array of called widgets
    public $_tree = []; // Only for catalog menus on footer and header. Minus one query

    // Instance method
    static function factory()
    {
        if (self::$_instance == NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     *  Get widget
     * @param  string $name [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string        [Widget HTML]
     */
    public static function get($name, $array = [], $save = true, $cache = false)
    {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

        if (APPLICATION == 'backend' && !Config::get('error')) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widgets::factory();
        }

        $_cache = Cache::instance();
        if ($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if ($save && isset($w->_data[$name])) {
                        $data = $w->_data[$name];
                    } else if (method_exists($w, $name)) {
                        $result = $w->$name($array);
                        if ($result !== null && $result !== false) {
                            $array = array_merge($array, $result);
                            $data = View::widget($array, $viewpath);
                        } else {
                            $data = null;
                        }
                    } else {
                        $data = $w->common($viewpath, $array);
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if ($_cache->get($name)) {
            $_cache->delete($name);
        }
        if ($save && isset($w->_data[$name])) {
            return $w->_data[$name];
        }
        if (method_exists($w, $name)) {
            $result = $w->$name($array);
            if ($result !== null && $result !== false) {
                if (is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget($array, $viewpath);
            } else {
                return $w->_data[$name] = null;
            }
        }
        return $w->_data[$name] = $w->common($viewpath, $array);
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     * @param  string $viewpath [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common($viewpath, $array)
    {
        if (file_exists(HOST . '/Views/Widgets/' . $viewpath . '.php')) {
            return View::widget($array, $viewpath);
        }
        return null;
    }

    public function Index_IndexTop(){
        $result = Common::factory('control')->getRow(1);
        $price_list = Common::factory('control')->getRow(11);
        if (!sizeof($result)) {
            return false;
        }
        return [
            'result' => $result,
            'price_list' => $price_list
        ];
    }

    public function Index_About(){
        $result = Common::factory('control')->getRow(12);
        $images = Common::getRowsFromTableByField('gallery_images', '6', 'gallery_id');

        if (!sizeof($result)) {
            return false;
        }
        return [
            'result' => $result,
            'images' => $images
        ];
    }

    public function Advantages(){
        $result = Common::factory('control')->getRow(4);
        if (!sizeof($result)) {
            return false;
        }
        return [
            'result' => $result,
        ];
    }

    public function Contact(){
        $result = Common::factory('control')->getRow(2);
        $bg_image = Common::factory('banners')->getRow(9);
        $offers = Common::getRowsFromTableByField('sliders', 'offer', 'type', 1);
        if (!sizeof($result)) {
            return false;
        }
        return [
            'result' => $result,
            'offers' => $offers,
            'bg_image' => $bg_image,
        ];
    }

    public function Sertificates(){
        $result = Common::factory('control')->getRow(3);
        $slides = Common::getRowsFromTableByField('sliders', 'sertificate', 'type', 1);
        if (!sizeof($result)) {
            return false;
        }
        return [
            'result' => $result,
            'slides' => $slides,
        ];
    }

    public function Logo(){
        $logo = Common::factory('banners')->getRow(8);
        if (!sizeof($logo)) {
            return false;
        }
        return [
            'logo' => $logo
        ];
    }
    public function Partners(){
        $partners = Common::getRowsFromTableByField('sliders', 'partner', 'type', 1);

        if (!sizeof($partners)) {
            return false;
        }
        return [
            'partners' => $partners
        ];
    }

    public function Index_Products(){
        $result = Common::factory('control')->getRow(9);
        $products = Common::getRowsFromTableByField('catalog', '55', 'parent_id', 1);
        if (!sizeof($result)) {
            return false;
        }
        return [
            'result' => $result,
            'products' => $products,
        ];
    }

    public function Footer()
    {
        $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort');
        $array['contentMenu'] = $contentMenu;
		$uri = explode('?', $_SERVER['REQUEST_URI']);
		$array['currentLink'] = $uri[0];
        return $array;
    }

    public function Header()
    {
        $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort');
        $array['contentMenu'] = $contentMenu;
        $array['user'] = User::info();
//        $array['countItemsInTheCart'] = Cart::factory()->_count_goods;
		$uri = explode('?', $_SERVER['REQUEST_URI']);
		$array['currentLink'] = $uri[0];
        return $array;
    }

    public function Head()
    {
        $styles = [
            HTML::media('css/jquery-plugins.css', false),
            HTML::media('css/style.css', false),
            HTML::media('css/init.css', false),
        ];
        $scripts = [
            HTML::media('js/programmer/translate-ru.js', false),
            HTML::media('js/modernizr.js', false),
            HTML::media('js/jquery.js', false),
            HTML::media('js/jquery-plugins.js', false),
            HTML::media('js/init.js', false),
            HTML::media('js/programmer/ajax.js', false),
            HTML::media('js/programmer/form-validation.js', false),
        ];
        $scripts_no_minify = [
//            HTML::media('js/programmer/ulogin.js', false),
        ];
        return ['scripts' => $scripts, 'styles' => $styles, 'scripts_no_minify' => $scripts_no_minify];
    }

}