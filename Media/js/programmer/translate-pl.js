var validationTranslate = {
	required: "To pole należy wypełnić!",
	required_checker: "Parametr ten jest wymagany!",
	required_select: "Parametr ten jest wymagany!",

	password: "Określ paroll!",
	remote: "Proszę, wprowadźcie prawidłowe znaczenie!",
	email: "Proszę wpisać poprawny adres e-mail!",
	url: "Proszę podać poprawny adres URL!",
	date: "Wpisz poprawną datę!",
	dateISO: "Proszę podać poprawną datę w formacie ISO!",
	number: "Proszę wpisać numery!",
	digits: "Proszę wpisać tylko liczby!",
	creditcard: "Proszę podać poprawny numer karty kredytowej!",
	equalTo: "Proszę ponownie wprowadzić wartość!",

	maxlength: "Proszę wpisać nie więcej niż {0} znaków!",
	maxlength_checker: "Proszę wybrać nie więcej niż {0} parametrów!",
	maxlength_select: "Proszę wybrać nie więcej niż {0} pozycji!",

	minlength: "Proszę podać co najmniej {0} znaków!",
	minlength_checker: "Wybierz co najmniej {0} opcje!",
	minlength_select: "Wybierz co najmniej {0} przedmioty!",

	rangelength: "Prosimy podać wartość pomiędzy {0} {1} znaków!",
	rangelength_checker: "Proszę wybrać od {0} do {1} parametrów!",
	rangelength_select: "Proszę wybrać od {0} do {1} punktów!",

	range: "Prosimy podać wartość między {0} - {1}!",
	max: "Prosimy podać wartość mniejsza lub równa {0}!",
	min: "Prosimy podać wartość większą lub równą {0}!",

	filetype: "Dopuszczalne rozszerzenia plików: {0}!",
	filesize: "Maksymalny rozmiar {0}KB!",
	filesizeEach: "Maksymalny rozmiar każdego plika {0}KB!",

	pattern: "Określ wartość odpowiadającą maski {0}!",
	word: "Wprowadź poprawne znaczenie słów!",
	noempty: "Pole nie może być puste!",
	login: "Proszę podać poprawną nazwę użytkownika!",
	phoneUA: "Nieprawidłowy format numeru telefonu ukraiński",
	phone: "Wprowadź korektno numer telefonu"
};

var mfpTranslate = {
	tClose: 'Zamknąć (ESC)',
	tLoading: 'Zawartość do treści ...',
	tNotFound: 'Nie znaleziono treści',
	tError: 'Niemożliwe do ściągnięcia <a href="%url%" target="_blank">treści</a>.',
	tErrorImage: 'Niemożliwe do ściągnięcia <a href="%url%" target="_blank">Obrazów #%curr%</a>.',
	tPrev: 'Poprzednia (klucz Left)',
	tNext: 'Następna (klucz Right)',
	tCounter: '%curr% z %total%'
};

var timelineTranslate = {
	"lang": "pl",
	"date": {
		"month_abbr": [
			"Sty.",
			"Lut.",
			"Mar.",
			"Kwi.",
			"Maj.",
			"Cze.",
			"Lip.",
			"Sie.",
			"Wrz.",
			"Paź.",
			"Lis.",
			"Gru."
		],
		"day_abbr": [
			"Nie.",
			"Pon.",
			"Wto.",
			"Śro.",
			"Czw.",
			"Pią.",
			"Sob."
		],
		"day": [
			"Niedziela",
			"Poniedziałek",
			"Wtorek",
			"Środa",
			"Czwartek",
			"Piątek",
			"Sobota"
		],
		"month": [
			"Stycznia",
			"Lutego",
			"Marca",
			"Kwietnia",
			"Maja",
			"Czerwca",
			"Lipca",
			"Sierpnia",
			"Września",
			"Października",
			"Listopada",
			"Grudnia"
		]
	},
	"api": {
		"wikipedia": "pl"
	},
	"messages": {
		"loading": "Ładowanie",
		"contract_timeline": "Zmniejsz Timeline",
		"return_to_title": "Wróć do tytułu",
		"wikipedia": "Z Wikipedii, wolnej encyklopedii",
		"loading_content": "Ładowanie zawartości",
		"expand_timeline": "Powiększ Timeline",
		"loading_timeline": "Ładowanie Timeline... "
	},
	"dateformats": {
		"full_long": "dddd',' d mmm yyyy 'um' HH:MM",
		"full_short": "d mmm",
		"full": "d mmmm yyyy",
		"month_short": "mmm",
		"time_no_seconds_small_date": "HH:MM'<br/><small>'d mmmm yyyy'</small>'",
		"month": "mmmm yyyy",
		"time_no_seconds_short": "HH:MM",
		"time_short": "HH:MM:ss",
		"year": "yyyy",
		"full_long_small_date": "HH:MM'<br/><small>'dddd',' d mmm yyyy'</small>'"
	}
};

var woldTranslate = {
	pl: {
		'title': 'Studio Wezom',
		'close': 'Zamknąć',
		'small': ' lub młodszy',
		'end-1': 'Strona może nie działać prawidłowo. Zalecamy używanie <b>%b</b>.</p>',
		'end-2': 'Strona może nie działać prawidłowo. Zalecamy używanie <b>%b</b>.</p>',
		'end-3': 'Strona może nie działać prawidłowo.</p>',
		'inform': '<p>Używasz starej przeglądarki - <b>%w</b>!',
		'device': '<p>Używasz - <b>%w</b>!',
		'old-os': '<p>Używasz przestarzałej system operacyjny - <b>%w</b>!',
		'ffx-esr': '<p>Używasz <b> <a href="https://www.mozilla.org/en-US/firefox/organizations/" title="Firefox - ESR" target="_blank">Firefox - ESR</a></b>!',
		'unknown': '<p>Korzystania z nieznanych nam robi!'
	}
}