<?php

return [
    'frontend' => [
        'Ajax', 'Index', 'User', 'Catalog', 'Sitemap', 'Contacts', 'Delivery', 'Price', 'Products', 'About', 'Content',
    ],
    'backend' => [
        'Ajax', 'Index', 'Catalog', 'Config', 'Contacts', 'Content', 'Log', 'Visitors', 'Blacklist',
        'MailTemplates', 'Menu', 'Seo', 'User', 'Sliders', 'Multimedia', 'Statistic', 'Crop',
        'Translates'
    ],
];