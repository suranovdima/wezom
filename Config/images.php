<?php
// Settings of images on the site
return [
    // Watermark path
    'watermark' => 'pic/logo.png',
    // Image types
    'types' => [
        'jpg', 'jpeg', 'png', 'gif',
    ],
    // Banners images
    'banners' => [
        [
            'path' => '',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Slider images
    'slider' => [
        [
            'path' => 'small',
            'width' => 200,
            'height' => 70,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 1460,
            'height' => 500,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Sliders images
    'sliders' => [
        [
            'path' => 'small',
            'width' => 352,
            'height' => 70,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 195,
            'height' => 273,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Sliders images
    'control' => [
        [
            'path' => 'small',
            'width' => 352,
            'height' => 70,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 195,
            'height' => 273,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Blog images
    'blog' => [
        [
            'path' => 'small',
            'width' => 200,
            'height' => 160,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 600,
            'height' => 400,
            'resize' => 1,
            'crop' => 0,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // News images
    'news' => [
        [
            'path' => 'small',
            'width' => 200,
            'height' => 160,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 600,
            'height' => NULL,
            'resize' => 1,
            'crop' => 0,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Brands images
    'brands' => [
        [
            'path' => 'original',
            'width' => 352,
            'height' => 70,
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Articles images
    'articles' => [
        [
            'path' => 'small',
            'width' => 200,
            'height' => 160,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 600,
            'height' => NULL,
            'resize' => 1,
            'crop' => 0,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    // Catalog groups images
    'catalog_tree' => [
        [
            'path' => '',
            'width' => 240,
            'height' => 240,
            'resize' => 1,
            'crop' => 1,
        ],
    ],
    // Products images
    'catalog' => [
        [
            'path' => 'small',
            'width' => 60,
            'height' => 60,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'medium',
            'width' => 592,
            'height' => 474,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 660,
            'height' => 286,
            'resize' => 1,
            'crop' => 0,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
    'gallery' => [
        [
            'path' => '',
            'width' => 200,
            'height' => 200,
            'resize' => 1,
            'crop' => 1,
        ],
    ],
    'gallery_images' => [
        [
            'path' => 'small',
            'width' => 200,
            'height' => 200,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'medium',
            'width' => 350,
            'height' => 350,
            'resize' => 1,
            'crop' => 1,
        ],
        [
            'path' => 'big',
            'width' => 1280,
            'height' => 1024,
            'resize' => 1,
            'crop' => 0,
        ],
        [
            'path' => 'slider',
            'width' => 750,
            'height' => 427,
            'resize' => 1,
            'crop' => 0,
        ],
        [
            'path' => 'original',
            'resize' => 0,
            'crop' => 0,
        ],
    ],
];