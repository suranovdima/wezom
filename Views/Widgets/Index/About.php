<?php
use Core\HTML;

?>
<div class="section2 pt-30 pb-50 pb-ms30">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="grid i-10">
				<div class="cell cell--24">
					<div class="section2__title revealator-slideright revealator-delay1 revealator-once">
						<span><?=$result->title_cust?></span>
					</div>
				</div>
				<div class="cell cell--24">
					<div class="section2__text revealator-slideleft revealator-delay2 revealator-once">
						<span><?=$result->name_page?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section2__block js-inview-div mt-30" data-background="Media/pic/section2-bg-1.png">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="grid grid--mdjcenter i-10">
					<div class="cell cell--12 cell--md24">
						<div class="section2__content mb-50 mb-md20">
							<div class="section2__quote revealator-slidedown revealator-delay4 revealator-once">
								<svg>
									<use xlink:href="Media/svg/sprite.svg#icon-quote" />
								</svg>
							</div>
							<div class="section2__desc view-text">
								<?=$result->text?>
							</div>
						</div>
						<div class="section2__btn">
							<div class="grid grid--smjcenter i-10">
								<div class="cell">
									<div class="btn btn--big revealator-slideright revealator-once js-link" data-link=".section6">
										<span>Оставить заявку</span>
									</div>
								</div>
								<div class="cell">
									<a href="<?=HTML::link('/about')?>" class="btn btn--big btn--light revealator-slideleft revealator-once">
										<span>Подробнее о компании</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="cell cell--12 cell--sm24">
						<div class="section2__slider js-section2-slider">
							<?php foreach($images as $image): ?>
								<div>
									<div class="section2__image">
										<div class="section2__image-inner">
											<!-- @TODO Изображение должно быть таких размеров: 595х374 -->
											<img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=HTML::media('images/gallery_images/original/'.$image->image, true, null, $image->image)?>" alt="">
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
