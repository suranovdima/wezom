<?php
use Core\HTML; ?>
<div class="section1 js-section">
	<div class="section1__bg">
		<img src="<?=HTML::media('images/control/original/'.$result->image, true, null, $result->image)?>" alt="">
	</div>
	<div class="section1__block">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="section1__grid grid grid--col grid--jcenter iv-15 iv-md10 js-section-block">
					<div class="cell cell--14 cell--md16 cell--ms18 cell--sm24">
						<div class="section1__title revealator-slideright revealator-delay1 revealator-once">
							<?=$result->text?>
						</div>
					</div>
					<div class="cell cell--11 cell--lg12 cell--md14 cell--ms16 cell--sm24">
						<div class="section1__text revealator-slideleft revealator-delay2 revealator-once">
							<span><?=$result->name_page?></span>
						</div>
					</div>
					<div class="cell cell--sm24">
						<div class="section1__btn revealator-slideright revealator-delay4 revealator-once">
							<a target="_blank" href="<?=HTML::media('pdfs/'.$price_list->pdf)?>" class="btn btn--big">
								<span>Просмотреть прайс-лист</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section1__bottom">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="section1__link js-link" data-link=".section2">
					<div class="section1__link-icon">
						<div class="section1__link-icon1">
							<svg>
								<use xlink:href="<?=HTML::media('svg/sprite.svg#arrow-down')?>" />
							</svg>
						</div>
						<div class="section1__link-icon2">
							<svg>
								<use xlink:href="<?=HTML::media('svg/sprite.svg#arrow-down')?>" />
							</svg>
						</div>
					</div>
					<div class="section1__link-text">
						<span>Подробнее</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>