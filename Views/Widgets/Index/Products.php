<?php
use Core\HTML; ?>
<div class="section3 pv-50 pv-ms30">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="grid i-10">
				<div class="cell cell--24">
					<div class="section3__title revealator-slideright revealator-delay1 revealator-once">
						<span><?=$result->title_cust?></span>
					</div>
				</div>
				<div class="cell cell--24">
					<div class="section3__text revealator-slideleft revealator-delay2 revealator-once">
						<span><?=$result->name_page?></span>
					</div>
				</div>
				<!-- @TODO Изображение должно быть таких размеров: 660х286 -->
				<?php foreach($products as $key=>$product):	?>
					<?php if($key < 5): ?>
						<div class="cell cell--<?php echo $key<2 ? 12 : 8 ; ?> cell--sm24">
							<a href="<?=HTML::link('/')?>" class="section3__item revealator-zoomin revealator-delay1 revealator-once">
								<div class="section3__image section3__image--big">
									<div class="section3__image-inner">
										<img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=HTML::media('images/catalog/big/'.$product->image, true, null, $product->image)?>" alt=""> </div>
								</div>
								<div class="section3__name">
									<span><?=$product->name?></span>
								</div>
							</a>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>


			</div>
		</div>
	</div>
</div>
