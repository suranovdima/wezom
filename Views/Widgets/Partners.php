<?php use Core\HTML; ?>
<div class="section7">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="section7__slider js-section7-slider">
				<?php foreach($partners as $partner): ?>
					<div>
						<div class="section7__item">
							<a href="<?=HTML::link($partner->link)?>" class="section7__image image image--contain" target="_blank">
								<!-- @TODO Изображение должно быть таких размеров: 352х70 -->
								<img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=HTML::media('images/sliders/original/'.$partner->image, true, null, $partner->image)?>" alt="">
							</a>
						</div>
					</div>
				<?php endforeach; ?>


			</div>
		</div>
	</div>
</div>
