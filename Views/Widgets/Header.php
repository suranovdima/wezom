<?php
use Core\HTML;
use Core\Config;
use Core\Widgets;

?>
<main>
<header>
    <div class="header-block">
        <div class="grid grid--xl pg-30">
            <div class="cell cell--24">
                <div class="grid grid--jbetween grid--aend grid--msacenter i-10">
                    <div class="cell cell--lg5 cell--sm6 cell--xs7">
                        <div class="header-block__logo pv-10">
                            <?php if($currentLink == '/'): ?>
                                <img src="<?php echo Widgets::get('Logo'); ?>" alt="">
                            <?php else: ?>
                               <a href="<?=HTML::link('/')?>"><img src="<?php echo Widgets::get('Logo'); ?>" alt=""></a>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="cell cell--lg19 cell--sm18 cell--xs17">
                        <div class="header-block__top pv-10">
                            <div class="grid grid--acenter grid--jend i-10">
                                <div class="cell cell--ms0">
                                    <a  href="tel:<?=Config::get('static.phone')?>" class="header-block__tel"><?=Config::get('static.phone')?></a>
                                </div>
                                <div class="cell cell--ms0">
                                    <div class="header-block__text">
                                        <span><?=Config::get('static.address')?></span>
                                    </div>
                                </div>
                                <div class="cell cell--md8 cell--ms cell--xs18 gcenter">
                                    <div class="btn js-mfp-ajax" data-url="<?=HTML::link('popup/callback')?>">
                                        <span>Заказать обратный звонок</span>
                                    </div>
                                </div>
                                <div class="cell cell--ms0">
                                    <div class="select js-select">
                                        <div class="select__head">
                                            <span>РУС</span>
                                            <svg>
                                                <use xlink:href="<?=HTML::media('svg/sprite.svg#arrow-down')?>" />
                                            </svg>
                                        </div>
                                        <div class="select__list">
                                            <a href="?lang=uk" class="select__item">
                                                <span>УКР</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--0 cell--ms">
                                    <a href="#mobile-menu" class="menu-link" title="Меню">
                                        <svg>
                                            <use xlink:href="<?=HTML::media('svg/sprite.svg#icon-list')?>" />
                                        </svg>
                                    </a>
                                    <div class="hide">
                                        <nav id="mobile-menu" class="mobile-menu js-mobile-menu is-first">
                                            <ul>
                                                <li class="mobile-menu__head">
															<span class="mobile-menu__left">
																<span class="mobile-menu__text">Навигация</span>
															</span>
															<span class="mobile-menu__right">
																<span class="mobile-menu__close js-menu-close">
																	<svg>
                                                                        <use xlink:href="<?=HTML::media('svg/sprite.svg#icon-close')?>" />
                                                                    </svg>
																</span>
															</span>
                                                </li>
                                                <?php foreach ($contentMenu as $obj): ?>
                                                    <li class="mobile-menu__item ">
                                                        <a <?php if ($currentLink != $obj->url and $currentLink != '/'.$obj->url): ?>href="<?=HTML::link($obj->url)?>"<?php endif;?>>
                                                            <span><?=$obj->name;?></span>
                                                        </a>
                                                    </li>
                                                <?php endforeach; ?>
                                                <li class="mobile-menu__item">
                                                    <a href="tel:<?=Config::get('static.phone')?>">
                                                        <span><?=Config::get('static.phone')?></span>
                                                    </a>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span><?=Config::get('static.address')?></span>
                                                </li>
                                                <li class="mobile-menu__block">
                                                    <span>Язык сайта</span>
                                                </li>
                                                <li class="mobile-menu__item is-active">
                                                    <span>РУС</span>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <a href="?lang=uk">
                                                        <span>УКР</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-menu pt-10 ms-hide">
                            <div class="grid grid--acenter grid--jend ig-25 ig-lg10">
                                <?php foreach ($contentMenu as $obj): ?>
                                    <a <?php if ($currentLink != $obj->url and $currentLink != '/'.$obj->url): ?>href="<?=HTML::link($obj->url)?>"<?php endif;?> class="main-menu__item <?php if($currentLink == $obj->url): ?>is-active<?php endif;?>">
                                        <span><?=$obj->name;?></span>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section>