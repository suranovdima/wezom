<?php
use Core\Widgets;
use Core\HTML;
use Core\Config;

?>
<?php  if(!Config::get('error') && $currentLink != '/sitemap' && !$text_flag){ echo Widgets::get('Partners'); } ?>
</section>
    <footer>
        <div class="move-top">
            <div class="grid grid--jcenter p-10">
                <div class="cell">
                    <div class="btn btn--transparent js-link" data-link="main">
                        <svg>
                            <use xlink:href="<?=HTML::media('svg/sprite.svg#arrow-up')?>" />
                        </svg>
                        <span>Вернуться наверх</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-block">
            <div class="grid grid--lg pg-30">
                <div class="cell cell--24">
                    <div class="grid grid--jbetween grid--smcol grid--smacenter i-10">
                        <div class="cell cell--4 cell--ms8 cell--sm sm-gcenter">
                            <div class="footer-block__logo">
                                <img src="<?php echo Widgets::get('Logo'); ?>" alt="">
                            </div>
                            <div class="footer-block__text">
                                <span><?=Config::get('static.footer_text')?></span>
                            </div>
                        </div>
                        <div class="cell cell--8 cell--ms0">
                            <div class="grid i-5">
                                <?php foreach ($contentMenu as $obj): ?>
                                    <div class="cell cell--12">
                                        <a <?php if ($currentLink != $obj->url and $currentLink != '/'.$obj->url): ?>href="<?=HTML::link($obj->url)?>"<?php endif;?> class="footer-block__link">
                                            <span><?=$obj->name;?></span>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="cell cell--6 cell--ms8 cell--sm12 cell--xs24">
                            <div class="grid grid--nowrap i-5">
                                <div class="cell cell--nogrow cell--noshrink">
                                    <div class="footer-block__icon">
                                        <svg>
                                            <use xlink:href="<?=HTML::media('svg/sprite.svg#icon-address')?>" />
                                        </svg>
                                    </div>
                                </div>
                                <div class="cell cell--grow">
                                    <div class="footer-block__label">
                                        <span>Адрес компании:</span>
                                    </div>
                                    <div class="footer-block__address">
                                        <p><?=Config::get('static.address_footer')?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--6 cell--ms8 cell--sm12 cell--xs24">
                            <div class="grid grid--nowrap i-5">
                                <div class="cell cell--nogrow cell--noshrink">
                                    <div class="footer-block__icon">
                                        <svg>
                                            <use xlink:href="<?=HTML::media('svg/sprite.svg#icon-phone')?>" />
                                        </svg>
                                    </div>
                                </div>
                                <div class="cell cell--grow">
                                    <div class="footer-block__label">
                                        <span>Дирекция:</span>
                                    </div>
                                    <div class="footer-block__address">
                                        <span>Тел: </span>
                                        <a href="tel:<?=Config::get('static.phone_ceo')?>" class="footer-block__tel"><?=Config::get('static.phone_ceo')?></a>
                                    </div>
                                    <div class="footer-block__label">
                                        <span>Отдел сбыта:</span>
                                    </div>
                                    <div class="footer-block__address">
                                        <span>Тел: </span>
                                        <a href="tel:<?=Config::get('static.phone')?>" class="footer-block__tel"><?=Config::get('static.phone')?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--24">
                            <div class="footer-block__line"></div>
                        </div>
                        <div class="cell cell--ms14 cell--sm">
                            <div class="footer-block__copyright">
                                <span><?=Config::get('static.copyright')?></span>
                                <a href="<?=HTML::link('/policy')?>" class="ms-block">Политика конфиденциальности</a>
                            </div>
                        </div>
                        <div class="cell">
                            <a href="<?=Config::get('static.footer_site_link')?>" class="footer-block__wezom" target="_blank">
                                <span>Агентство Wezom</span>
                                <svg>
                                    <use xlink:href="<?=HTML::media('svg/sprite.svg#wezom-logo')?>" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <svg class="clip-path">
            <clipPath id="icon-triangle-right">
                <path d="M395,0h825v680H0L395,0z"></path>
            </clipPath>
            <clipPath id="icon-triangle-left">
                <path d="M0,0h1220L848,640H0V0z"></path>
            </clipPath>
        </svg>
    </footer>
</main>

<script>
    var wConfig = {
        svgSprite: '<?=HTML::media('svg/sprite.svg')?>',
        cart: {
            actions: {
                list: {
                    update: {
                        url: 'hidden/cart-update.php'
                    },
                    change: {
                        url: 'hidden/cart-change.php'
                    },
                    add: {
                        url: 'hidden/cart-add.php'
                    },
                    remove: {
                        url: 'hidden/cart-remove.php'
                    },
                }
            },
            templates: {
                list: {
                    window: {
                        url: 'templates/cart-window.ejs'
                    },
                    popup: {
                        url: 'templates/cart-popup.ejs'
                    },
                    order: {
                        url: 'templates/cart-order.ejs'
                    }
                }
            }
        }
    };
</script>

<!-- @TODO @all Изменить путь к изображению на относительный от корня сайта, в переменной `$wzmOld_URL_IMG` -->
<script>var $wzmOld_URL_IMG = '<?=HTML::media('pic/wezom-info-red.gif')?>';</script>
<script async="async" defer="defer" src="<?=HTML::media('js/wold.js')?>"></script>

<!-- noscript-msg -->
<noscript>
    <link rel="stylesheet" href="<?=HTML::media('css/noscript-msg.css')?>">
    <input id="noscript-msg__close" type="checkbox" title="Закрити">
    <div id="noscript-msg">
        <label id="noscript-msg__times" for="noscript-msg__close" title="Закрити">&times;</label>
        <a href="http://wezom.com.ua/" target="_blank" title="Cтудія Wezom" id="noscript-msg__link">&nbsp;</a>
        <div id="noscript-msg__block">
            <div id="noscript-msg__text">
                <p>В Вашем браузере отключен
                    <strong>JavaScript</strong>! Для корректной работы с сайтом необходима поддержка Javascript.</p>
                <p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>
            </div>
        </div>
    </div>
</noscript>
