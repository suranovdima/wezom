<?php use Core\HTML; ?>
<div class="section5 pv-50 pv-ms30">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="grid grid--col grid--acenter i-10">
				<div class="cell">
					<div class="section5__title revealator-slideright revealator-delay1 revealator-once">
						<span><?=$result->title_cust?></span>
					</div>
				</div>
				<div class="cell">
					<div class="section5__line revealator-slideleft revealator-delay2 revealator-once"></div>
				</div>
				<div class="cell">
					<div class="section5__text revealator-slideright revealator-delay2 revealator-once">
						<span><?=$result->name_page?></span>
					</div>
				</div>
				<div class="cell cell--24">
					<div class="section5__slider js-section5-slider js-mfp-container">
						<?php foreach($slides as $slide): ?>
							<div>
								<div class="section5__item">
									<div class="section5__link js-mfp-image" data-url="Media/images/section5-certificate-2.jpg">
										<div class="section5__image">
											<div class="section5__image-inner">
												<img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=HTML::media('images/sliders/big/'.$slide->image, true, null, $slide->image)?>" alt="">
											</div>
										</div>
										<div class="section5__name">
											<span><?=$slide->name?></span>
										</div>
									</div>
									<?php if($slide->pdf):?>
										<div class="section5__btn">
											<div class="btn">
												<svg>
													<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-download')?>" />
												</svg>
												<a target="_blank" href="<?=HTML::media('pdfs/'.$slide->pdf)?>"><span>Загрузить</span></a>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
