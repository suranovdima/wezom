<?php use Core\HTML; ?>
<div class="section6">
	<div class="section6__block js-inview-div" data-background="<?=HTML::media('images/banners/'.$bg_image->image, true, null, $bg_image->image)?>">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="grid grid--acenter i-10 js-section-block">
					<div class="cell cell--12 cell--sm24">
						<div class="grid grid--col iv-15 iv-md10">
							<div class="cell cell--16 cell--sm24">
								<div class="section6__title revealator-slideright revealator-delay1 revealator-once">
									<span>Оставить заявку</span>
								</div>
							</div>
							<div class="cell cell--16 cell--sm24">
								<div class="section6__text revealator-slideleft revealator-delay2 revealator-once">
									<span><?=$result->text_for_widget?></span>
								</div>
							</div>
						</div>
					</div>
					<div class="cell cell--12 cell--sm24">
						<div class="grid grid--jcenter">
							<div class="cell cell--13 cell--xl14 cell--md16 cell--sm24">
								<div class="section6__form form js-form" data-errors-by-step="true" data-form="true" data-ajax="contacts">
									<div class="grid grid--smjcenter i-10">
										<div class="cell cell--24">
											<div class="form__label">Продукты и услуги *</div>
											<div class="control-holder control-holder--text">
												<select data-name="type" name="type" class="js-select2" required="">
													<option value="">Выбрать</option>
													<?php foreach($offers as $offer): ?>
													<option value="<?=$offer->id?>"><?=$offer->name?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="cell cell--24">
											<div class="form__label">Ваше имя *</div>
											<div class="control-holder control-holder--text">
												<input type="text" name="name" data-name="name" required="" data-rule-minlength="2" data-rule-word="true">
											</div>
										</div>
										<div class="cell cell--24">
											<div class="form__label">Номер телефона *</div>
											<div class="control-holder control-holder--text">
												<input type="tel" name="tel" data-name="tel" required="" data-rule-phone="true" class="js-inputmask" data-mask="+38(099)999-99-99">
											</div>
										</div>
										<div class="cell cell--24">
											<div class="control-holder control-holder--flag">
												<label>
													<input type="checkbox" name="agree" data-name="agree" value="1" required>
													<ins>
														<svg>
															<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-check')?>" />
														</svg>
													</ins>
																<span>Я согласен с
																	<a href="<?=HTML::link('/policy')?>" class="form__link" target="_blank">политикой конфиденциальности</a>
																</span>
												</label>
												<div>
													<label for="agree" class="has-error" style="display: none;"></label>
												</div>
												<?php if (array_key_exists('token', $_SESSION)): ?>
													<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>"/>
												<?php endif; ?>
											</div>
										</div>
										<div class="cell cell--18 cell--sm24">
											<div class="btn btn--full js-form-submit">
												<span>Оставить заявку</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell cell--12">

			</div>
		</div>
	</div>
</div>
