<?php 
	use Core\HTML;
	use Core\View;
?>
<nav class="sitemap">
	<ul>
		<?php foreach($result[0] as $value): ?>
		<li>
			<a href="<?=HTML::link($value->alias)?>"><?=$value->name?></a>
		<?php if($result[$value->id] !== NULL):
			echo '<ul>';
			foreach($result[$value->id] as $element): ?>
				<li>
					<a href="<?=HTML::link($element->alias)?>"><?=$element->name?></a>
				</li>
				<?php
			endforeach;
			echo '</ul>';
		endif;?>
		</li>
		<?php endforeach; ?>
	</ul>
</nav>