<?php
use Core\Widgets;
use Core\HTML;
?>
<div class="section8  js-section">
	<div class="section8__right">
		<div class="section8__right-inner">
			<div class="section8__right-svg">
				<svg viewBox="0 0 1220 680">
					<!-- @TODO Изображение должно быть таких размеров: 1220х680 -->
					<image xlink:href="<?=HTML::media('images/control/original/'.$current->image, true, null, $current->image)?>" x="0" y="0" width="1220" height="680" clip-path="url(#icon-triangle-right)"></image>
				</svg>
			</div>
		</div>
	</div>
	<div class="section8__left">
		<div class="section8__left-inner">
			<div class="section8__left-svg">
				<svg>
					<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-triangle-left')?>" />
				</svg>
			</div>
		</div>
	</div>
	<div class="section8__block">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="grid grid--col grid--nowrap grid--jcenter iv-15 iv-md10 iv-ms5 js-section-block">
					<div class="cell cell--14 cell--md13 cell--sm24">
						<div class="section8__title revealator-slideright revealator-delay1 revealator-once">
							<span><?=$current->title_top?></span>
						</div>
					</div>
					<div class="cell cell--11 cell--lg12 cell--md112 cell--sm24">
						<div class="section8__text revealator-slideleft revealator-delay2 revealator-once">
							<span><?=$current->text_top?></span>
						</div>
					</div>
					<div class="cell cell--sm24">
						<div class="section8__btn revealator-slideright revealator-delay4 revealator-once">
							<div class="btn btn--big js-link" data-link=".section6">
								<span>Оставить заявку</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section10 pv-50 pv-lg30">
	<div class="grid i-25 i-lg15">
		<?php foreach($products as $product): ?>
			<div class="cell cell--24">
				<div class="section10__item js-mfp-container">
					<div class="grid grid--lg pg-30">
						<div class="cell cell--24">
							<div class="grid grid--jcenter iv-15 ig-25 ig-lg15">
								<div class="cell cell--24">
									<div class="section10__title revealator-slideright revealator-delay1 revealator-once">
										<span><?=$product->name?></span>
									</div>
								</div>
								<div class="cell cell--12 cell--sm24">
									<div class="section10__image revealator-zoomin revealator-delay1 revealator-once">
										<!-- @TODO Изображение должно быть самое большое, которое есть -->
										<div class="section10__image-item js-mfp-image" data-url="<?=HTML::media('images/catalog/medium/'.$product->image, true, null, $product->image)?>">
											<!-- @TODO Изображение должно быть таких размеров: 592х474 -->
											<img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=HTML::media('images/catalog/medium/'.$product->image, true, null, $product->image)?>" alt=""> </div>
									</div>
								</div>
								<div class="cell cell--12 cell--ms24">
									<div class="grid i-15">
										<div class="cell cell--24">
											<div class="section10__desc">
												<p><?=$product->product_description?></p>
											</div>
										</div>
										<div class="cell cell--24">
											<div class="section10__line"></div>
										</div>
										<div class="cell cell--24">
											<div class="grid grid--acenter grid--jbetween i-10">
												<div class="cell">
													<div class="section10__price">
														<span>Стоимость</span>
														<div>от <?=$product->min_price?> м3/грн</div>
													</div>
												</div>

												<?php if($product->pdf != null): ?>
												<div class="cell">
													<a href="<?=HTML::media('pdfs/'.$product->pdf)?>" target="_blank" class="section10__link-block">
														<div class="grid grid--acenter grid--nowrap i-10">
															<div class="cell cell--nogrow cell--noshrink">
																<div class="section10__icon">
																	<svg>
																		<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-download')?>" />
																	</svg>
																</div>
															</div>
															<div class="cell cell--grow">
																<div class="section10__link">
																	<span>Скачать прайс-лист</span>
																</div>
																<div class="section10__info">
																	<span>PDF, <?=$product->size_of_pdf?></span>
																</div>
															</div>
														</div>
													</a>
												</div>
												<?php endif; ?>
												<div class="cell cell--24">
													<div class="btn btn--big btn--full js-link" data-link=".section6">
														<span>Оставить заявку</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="section10__line mt-50 mt-lg30"></div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>