<?php
use Core\HTML;
?>
<div class="section13 section13--background js-section">
	<div class="section13__block">
		<div class="grid grid--col grid--nowrap grid--jcenter iv-15 iv-md10 iv-ms5 js-section-block">
			<div class="cell cell--24">
				<div class="section13__title revealator-slideright revealator-delay1 revealator-once">
					<span><?=$current->title_top?></span>
				</div>
			</div>
			<div class="cell cell--24">
				<div class="section13__text revealator-slideleft revealator-delay2 revealator-once">
					<span><?=$current->text_top?></span>
				</div>
			</div>
			<div class="cell cell--sm24">
				<div class="section13__btn revealator-slideright revealator-delay4 revealator-once">
					<div class="btn btn--big js-link" data-link=".section14">
						<span>Написать нам</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section13__right">
		<div class="section13__right-inner">
			<div class="hide js-map-script" data-key="AIzaSyDL6xIhFeOJeE9nXsObhPKfD1wRV4xFknE" data-language="ru" data-marker="<?=HTML::media('pic/map-marker.png')?>"></div>
			<div class="section13__right-map js-map" data-lat="51.5155164" data-lng="30.7577667" data-address="<?=$current->name_page?>"></div>
			<div class="section13__right-triangle">
				<img src="<?=HTML::media('pic/icon-triangle-right-small.png')?>" alt="">
			</div>
		</div>
	</div>
	<div class="section13__left">
		<div class="section13__left-inner">
			<div class="section13__left-svg">
				<svg viewBox="0 0 1220 640">
					<image xlink:href="<?=HTML::media('images/control/original/'.$current->image, true, null, $current->image)?>" x="0" y="0" width="1220" height="640" clip-path="url(#icon-triangle-left)"></image>
					<path fill="#2A3343" opacity="0.7" d="M0,0h1220L848,640H0V0z" />
				</svg>
			</div>
		</div>
	</div>
</div>
<div class="section14 pv-50 pv-lg30">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="grid i-20">
				<?=$current->text?>
				<div class="cell cell--12 cell--sm24">
					<div class="grid i-20 i-sm10">
						<div class="cell cell--24">
							<div class="section14__title">
								<span>Контактная форма</span>
							</div>
							<div class="section14__line mt-20"></div>
						</div>
						<div class="cell cell--24">
							<div class="section14__form form js-form" data-form="true" data-errors-by-step="true" data-ajax="contacts">
								<div class="grid grid--acenter i-10">
									<div class="cell cell--12 cell--ms24">
										<div class="grid i-10">
											<div class="cell cell--24">
												<div class="form__label">Продукты и услуги *</div>
												<div class="control-holder control-holder--text">
													<select name="type" class="js-select2" required="">
														<option value="">Выбрать</option>
														<?php foreach($offers as $offer): ?>
															<option value="<?=$offer->id?>"><?=$offer->name?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="cell cell--24">
												<div class="form__label">Ваше имя *</div>
												<div class="control-holder control-holder--text">
													<input type="text" name="name" data-name="name" required="" data-rule-minlength="2" data-rule-word="true">
												</div>
											</div>
											<div class="cell cell--24">
												<div class="form__label">Номер телефона *</div>
												<div class="control-holder control-holder--text">
													<input type="tel" name="tel" data-name="tel" required="" data-rule-phone="true" class="js-inputmask" data-mask="+38(099)999-99-99">
												</div>
											</div>
											<?php if (array_key_exists('token', $_SESSION)): ?>
												<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>"/>
											<?php endif; ?>
										</div>
									</div>
									<div class="cell cell--12 cell--ms24">
										<div class="form__label">Сообщение *</div>
										<div class="control-holder control-holder--text">
											<textarea name="text" data-name="text" required="" data-rule-minlength="10"></textarea>
										</div>
									</div>
									<div class="cell cell--14 cell--lg12 cell--ms24">
										<div class="control-holder control-holder--flag">
											<label>
												<input type="checkbox" name="agree" data-name="agree" value="1" required>
												<ins>
													<svg>
														<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-check')?>" />
													</svg>
												</ins>
															<span>Я согласен с
																<a href="<?=HTML::link('/policy')?>" class="form__link" target="_blank">политикой конфиденциальности</a>
															</span>
											</label>
											<div>
												<label for="agree" class="has-error" style="display: none;"></label>
											</div>
										</div>
									</div>
									<div class="cell cell--10 cell--lg12 cell--ms24">
										<div class="btn btn--full btn--big js-form-submit">
											<span>Отправить</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
