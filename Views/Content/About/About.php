<?php use Core\HTML; ?>
<div class="section8 section8--background js-section">
	<div class="section8__right">
		<div class="section8__right-inner">
			<div class="section8__right-svg">
				<svg viewBox="0 0 1220 680">
					<!-- @TODO Изображение должно быть таких размеров: 1220х680 -->
					<image xlink:href="<?=HTML::media('images/control/original/'.$current->image, true, null, $current->image)?>" x="0" y="0" width="1220" height="680" clip-path="url(#icon-triangle-right)"></image>
				</svg>
			</div>
		</div>
	</div>
	<div class="section8__left">
		<div class="section8__left-inner">
			<div class="section8__left-svg">
				<svg>
					<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-triangle-left')?>" />
				</svg>
			</div>
		</div>
	</div>
	<div class="section8__block">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="grid grid--col grid--nowrap grid--jcenter iv-15 iv-md10 iv-ms5 js-section-block">
					<div class="cell cell--14 cell--md13 cell--sm24">
						<div class="section8__title revealator-slideright revealator-delay1 revealator-once">
							<?=$current->title_top?>
						</div>
					</div>
					<div class="cell cell--11 cell--lg12 cell--md112 cell--sm24">
						<div class="section8__text revealator-slideleft revealator-delay2 revealator-once">
							<span><?=$current->text_top?></span>
						</div>
					</div>
					<div class="cell cell--sm24">
						<div class="section8__btn revealator-slideright revealator-delay4 revealator-once">
							<div class="btn btn--big js-link" data-link=".section6">
								<span>Оставить заявку</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section9 pv-50 pv-ms30">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="grid i-10 js-mfp-container">
				<div class="cell cell--24">
					<div class="section9__title revealator-slideright revealator-delay1 revealator-once">
						<span><?=$current->title_cust?></span>
					</div>
				</div>
				<div class="cell cell--24">
					<div class="section9__text revealator-slideleft revealator-delay2 revealator-once">
						<span><?=$current->name_page?></span>
					</div>
				</div>
				<div class="cell cell--24">
					<div class="section9__desc view-text mv-30 mv-lg0">
						<?=$current->text?>
					</div>
				</div>
				<?php foreach($images as $image): ?>
					<div class="cell cell--8 cell--md12 cell--sm24">
						<div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
							<!-- @TODO Изображение должно быть самое большое, которое есть -->
							<div class="section9__image js-mfp-image" data-url="<?=HTML::media('images/gallery_images/original/'.$image->image, true, null, $image->image)?>">
								<div class="section9__image-inner">
									<!-- @TODO Изображение должно быть таких размеров: 650х427 -->
									<img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?=HTML::media('images/gallery_images/slider/'.$image->image, true, null, $image->image)?>" alt=""> </div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</div>