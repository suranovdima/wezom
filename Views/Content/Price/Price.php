<?php
use Core\Widgets;
use Core\HTML;
?>
<div class="section8 section8--background js-section">
	<div class="section8__right">
		<div class="section8__right-inner">
			<div class="section8__right-svg">
				<svg viewBox="0 0 1220 680">
					<!-- @TODO Изображение должно быть таких размеров: 1220х680 -->
					<image xlink:href="<?=HTML::media('images/control/original/'.$current->image, true, null, $current->image)?>" x="0" y="0" width="1220" height="680" clip-path="url(#icon-triangle-right)"></image>
				</svg>
			</div>
		</div>
	</div>
	<div class="section8__left">
		<div class="section8__left-inner">
			<div class="section8__left-svg">
				<svg>
					<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-triangle-left')?>" />
				</svg>
			</div>
		</div>
	</div>
	<div class="section8__block">
		<div class="grid grid--lg pg-30">
			<div class="cell cell--24">
				<div class="grid grid--col grid--nowrap grid--jcenter iv-15 iv-md10 iv-ms5 js-section-block">
					<div class="cell cell--14 cell--md13 cell--sm24">
						<div class="section8__title revealator-slideright revealator-delay1 revealator-once">
							<?=$current->title_top?>
						</div>
					</div>
					<div class="cell cell--11 cell--lg12 cell--md112 cell--sm24">
						<div class="section8__text revealator-slideleft revealator-delay2 revealator-once">
							<span><?=$current->text_top?></span>
						</div>
					</div>
					<div class="cell cell--sm24">
						<div class="section8__btn revealator-slideright revealator-delay4 revealator-once">
							<div class="btn btn--big js-link" data-link=".section6">
								<span>Оставить заявку</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section11 pv-50 pv-lg30">
	<div class="grid grid--lg pg-30">
		<div class="cell cell--24">
			<div class="grid iv-15 ig-25 ig-lg15">
				<div class="cell cell--24">
					<div class="section11__title">
						<span>Цены на нашу продукцию</span>
					</div>
				</div>
				<div class="cell cell--16 cell--lg14 cell--ms24">
					<div class="table-wrapper js-table-wrapper">
						<div class="table-wrapper__holder js-table-wrapper__holder">
							<table class="table-wrapper__table js-table-wrapper__table">
								<thead>
								<tr>
									<td>
										<span>Перечень товаров</span>
									</td>
									<td>
										<span>Стоимость при заказе до 30 м3, м3/грн</span>
									</td>
									<td>
										<span>Стоимость при заказе 30-60 м3, м3/грн</span>
									</td>
									<td>
										<span>Стоимость при заказе 60-120 м3, м3/грн</span>
									</td>
								</tr>
								</thead>
								<tbody>
								<?php foreach($products as $product): ?>
									<tr>
										<td>
											<span><?=$product->name?></span>
										</td>
										<td>
											<span><?=$product->cost?></span>
										</td>
										<td>
											<span><?=$product->cost1?></span>
										</td>
										<td>
											<span><?=$product->cost2?></span>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="cell cell--8 cell--lg10 cell--ms24">
					<div class="section11__right mb-50 mb-lg30">
						<div class="grid i-20">
							<?php if($current->pdf): ?>
							<div class="cell cell--24">
								<a href="<?=HTML::media('pdfs/'.$current->pdf)?>" target="_blank" class="section11__link-block">
									<div class="grid grid--acenter grid--nowrap i-10">
										<div class="cell cell--nogrow cell--noshrink">
											<div class="section11__icon">
												<svg>
													<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-download')?>" />
												</svg>
											</div>
										</div>
										<div class="cell cell--grow">
											<div class="section11__link">
												<span>Скачать прайс-лист</span>
											</div>
											<div class="section11__info">
												<span>PDF, <?=$current->size_of_pdf?></span>
											</div>
											<div class="section11__line"></div>
										</div>
									</div>
								</a>
							</div>
							<?php endif; ?>
							<?php if($current->xls): ?>
							<div class="cell cell--24">
								<a href="<?=HTML::media('xls/'.$current->xls)?>" target="_blank" class="section11__link-block">
									<div class="grid grid--acenter grid--nowrap i-10">
										<div class="cell cell--nogrow cell--noshrink">
											<div class="section11__icon">
												<svg>
													<use xlink:href="<?=HTML::media('svg/sprite.svg#icon-download')?>" />
												</svg>
											</div>
										</div>
										<div class="cell cell--grow">
											<div class="section11__link">
												<span>Скачать прайс-лист</span>
											</div>
											<div class="section11__info">
												<span>XLS, <?=$current->size_of_xls?></span>
											</div>
										</div>
									</div>
								</a>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="section11__desc view-text">
						<?=$current->text?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
