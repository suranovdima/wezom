<?php
use Core\Widgets;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">

<head>
	<?php echo Widgets::get('Head', $_seo); ?>
	<?php foreach ($_seo['scripts']['head'] as $script): ?>
		<?php echo $script; ?>
	<?php endforeach ?>
	<?php echo $GLOBAL_MESSAGE; ?>
</head>

<body class="page-<?=$current->alias?>">
<?php echo Widgets::get('Header', ['config' => $_config]); ?>
<?php
	echo $_content;
	switch($current->id){
		case 12 : //About
			echo Widgets::get('Advantages');
			echo Widgets::get('Sertificates');
			echo Widgets::get('Contact');
			break;
		case 9: // Products
			echo Widgets::get('Contact');
			break;
		case 11: //Price
			echo Widgets::get('Contact');
			break;
		case 5: //Delivery
			echo Widgets::get('Contact');
			break;
	}
?>
<?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
</body>

</html>
