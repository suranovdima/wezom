<?php
use Core\Widgets;
use Core\HTML;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">

<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>

<body class="page-text">
    <?php echo Widgets::get('Header', ['config' => $_config]); ?>
        <div class="page pv-30">
            <div class="grid grid--md pg-30">
                <div class="cell cell--24">
                    <div class="page__title">
                        <span><?=$_seo['h1']?></span>
                    </div>
                    <div class="page__content view-text">
                        <?=$_content?>
                    </div>
                </div>
            </div>
        </div>
    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
</body>

</html>
