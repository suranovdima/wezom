<?php use Core\Widgets; ?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">

<head>
    <?php echo Widgets::get('Head', $_seo); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>

<body class="page-index">
    <?php echo Widgets::get('Header', ['config' => $_config]); ?>

        <?php echo Widgets::get('Index_IndexTop'); ?>
        <?php echo Widgets::get('Index_About'); ?>
        <?php echo Widgets::get('Index_Products'); ?>
        <?php echo Widgets::get('Advantages'); ?>
        <?php echo Widgets::get('Sertificates'); ?>
        <?php echo Widgets::get('Contact'); ?>

    <?php echo Widgets::get('Footer', ['counters' => Core\Arr::get($_seo, 'scripts.counter'), 'config' => $_config]); ?>
</body>

</html>
