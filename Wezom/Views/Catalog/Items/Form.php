<?php
use Core\HTML;
echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-6">
        <div class="widget box">
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <ul class="liTabs t_wrap">
                        <li class="t_item">
                            <a class="t_link" href="#"><?php echo __('Основные данные'); ?></a>
                            <div class="t_content">
                                <div class="form-group">
                                    <div class="rowSection">
                                        <div class="col-md-4">
                                            <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="rowSection">
                                        <div class="col-md-6 form-group">
                                            <?php echo \Forms\Builder::select('<option value="0">' . __('Не выбрано') . '</option>'.$tree,
                                                NULL, [
                                                    'id' => 'parent_id',
                                                    'name' => 'FORM[parent_id]',
                                                    'class' => 'valid',
                                                ], [
                                                    'text' => __('Группа'),
                                                    'tooltip' => '<b>' . __('При изменении группы товара меняется набор характеристик!') . '</b>',
                                                ]); ?>
                                        </div>
<!--                                        <div class="col-md-6 form-group">-->
<!--                                            --><?php //echo \Forms\Builder::input([
//                                                'name' => 'FORM[sort]',
//                                                'value' => $obj->sort,
//                                            ], [
//                                                'text' => __('Позиция'),
//                                                'tooltip' => '<b>' . __('Поле определяет позицию товара среди других в списках') . '</b>',
//                                            ]); ?>
<!--                                        </div>-->
                                    </div>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    --><?php //echo \Forms\Builder::input([
//                                        'name' => 'FORM[artikul]',
//                                        'value' => $obj->artikul,
//                                    ], __('Артикул')); ?>
<!--                                </div>-->
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[name]',
                                        'value' => $obj->name,
                                        'class' => ['valid', 'translitSource'],
                                    ], __('Название')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::alias([
                                        'name' => 'FORM[alias]',
                                        'value' => $obj->alias,
                                        'class' => 'valid',
                                    ], [
                                        'text' => __('Алиас'),
                                        'tooltip' => __('<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>'),
                                    ]); ?>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <div class="col-md-3">-->
<!--                                        --><?php //echo \Forms\Builder::bool($obj->sale, 'sale', __('Акция')); ?>
<!--                                    </div>-->
<!--                                    <div class="col-md-9">-->
<!--                                        <label class="control-label">--><?php //echo __('Наличие'); ?><!--</label>-->
<!--                                        <div class="">-->
<!--                                            <label class="checkerWrap-inline">-->
<!--                                                --><?php //echo \Forms\Builder::radio(($obj && $obj->available == 0) ? true : false, [
//                                                    'name' => 'available',
//                                                    'value' => 0,
//                                                ]); ?>
<!--                                                --><?php //echo __('Нет в наличии'); ?>
<!--                                            </label>-->
<!--                                            <label class="checkerWrap-inline">-->
<!--                                                --><?php //echo \Forms\Builder::radio((!$obj || $obj->available == 1) ? true : false, [
//                                                    'name' => 'available',
//                                                    'value' => 1,
//                                                ]); ?>
<!--                                                --><?php //echo __('Есть в наличии'); ?>
<!--                                            </label>-->
<!--                                            <label class="checkerWrap-inline">-->
<!--                                                --><?php //echo \Forms\Builder::radio(($obj && $obj->available == 2) ? true : false, [
//                                                    'name' => 'available',
//                                                    'value' => 2,
//                                                ]); ?>
<!--                                                --><?php //echo __('Под заказ'); ?>
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="form-group costField">
                                    <?php echo \Forms\Builder::input([
                                        'type' => 'tel',
                                        'name' => 'FORM[cost]',
                                        'value' => $obj->cost,
                                        'class' => 'valid',
                                    ], __('Цена при заказе до 30 м3, м3/грн')); ?>
                                </div>
                                <div class="form-group costField">
                                    <?php echo \Forms\Builder::input([
                                        'type' => 'tel',
                                        'name' => 'FORM[cost1]',
                                        'value' => $obj->cost1,
                                        'class' => 'valid',
                                    ], __('Цена при заказе 30-60 м3, м3/грн')); ?>
                                </div>
                                <div class="form-group costField">
                                    <?php echo \Forms\Builder::input([
                                        'type' => 'tel',
                                        'name' => 'FORM[cost2]',
                                        'value' => $obj->cost2,
                                        'class' => 'valid',
                                    ], __('Цена при заказе 60-120 м3, м3/грн')); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::textarea([
                                        'name' => 'FORM[product_description]',
                                        'value' => $obj->product_description,
                                        'class' => 'valid',
                                    ], __('Описание товара')); ?>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo __('Файл'); ?> (.pdf)</label>
                                        <?php if ($obj->pdf != NULL): ?>
                                            <a class="btn" href='<?=HTML::media('pdfs/'.$obj->pdf)?>' target='_blank'>Просмотреть загруженный файл</a><br><br>
                                            <a class="btn" href='/wezom/items/delete_file/<?=$obj->id?>'>Удалить файл</a><br>
                                            <br><input type="hidden" name="FORM[pdf]" value="<?=$obj->pdf?>" />
                                        <?php else: ?>
                                            <input type="file" name="pdf" accept="application/pdf" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group hiddenCostField" <?php echo !$obj->sale ? 'style="display:none;"' : ''; ?>>
                                    <?php echo \Forms\Builder::input([
                                        'type' => 'tel',
                                        'name' => 'FORM[cost_old]',
                                        'value' => $obj->cost_old,
                                    ], __('Старая цена')); ?>
                                </div>
                            </div>
                        </li>
                        <li class="t_item">
                            <a class="t_link" href="#">Мета-данные</a>
                            <div class="t_content">
                                <div style="font-weight: bold; margin-bottom: 10px;"><?php echo __('Внимание! Незаполненные данные будут подставлены по шаблону', [':link' => \Core\HTML::link('wezom/seo_templates/edit/2')]); ?></div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[h1]',
                                        'value' => $obj->h1,
                                    ], [
                                        'text' => 'H1',
                                        'tooltip' => __('Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title'),
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::input([
                                        'name' => 'FORM[title]',
                                        'value' => $obj->title,
                                    ], [
                                        'text' => 'Title',
                                        'tooltip' => __('<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>'),
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::textarea([
                                        'name' => 'FORM[keywords]',
                                        'rows' => 5,
                                        'value' => $obj->keywords,
                                    ], [
                                        'text' => 'Keywords',
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo \Forms\Builder::textarea([
                                        'name' => 'FORM[description]',
                                        'value' => $obj->description,
                                        'rows' => 5,
                                    ], [
                                        'text' => 'Description',
                                    ]); ?>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>

<?php echo $uploader; ?>
<?php echo $related; ?>

<script>
    $(function(){
        $('input[name="sale"]').on('change', function(){
            var val = parseInt( $(this).val() );
            if( val ) {
                var cost = $('input[name="FORM[cost]"]').val();
                var cost_old = $('input[name="FORM[cost_old]"]').val();
                $('input[name="FORM[cost]"]').val(cost_old);
                $('input[name="FORM[cost_old]"]').val(cost);
                $('.hiddenCostField').show();
            } else {
                var cost = $('input[name="FORM[cost]"]').val();
                var cost_old = $('input[name="FORM[cost_old]"]').val();
                $('input[name="FORM[cost]"]').val(cost_old);
                $('input[name="FORM[cost_old]"]').val(cost);
                $('.hiddenCostField').hide();
            }
        });

        $('#parent_id').on('change', function(){
            var catalog_tree_id = $(this).val();
            $.ajax({
                url: '/wezom/ajax/catalog/getSpecificationsByCatalogTreeID',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    catalog_tree_id: catalog_tree_id
                },
                success: function(data){
                    var i = 0;
                    var j = 0;
                    var val;
                    var html = '<option value="0"><?php echo __('Нет'); ?></option>';
                    $('#model_alias').html(html);
                    for(i = 0; i < data.brands.length; i++) {
                        html += '<option value="' + data.brands[i].alias + '">' + data.brands[i].name + '</option>';
                    }
                    $('#brand_alias').html(html);
                    html = '';
                    for(i = 0; i < data.specifications.length; i++) {
                        var spec = data.specifications[i];
                        if( data.specValues[spec.id] ) {
                            var values = data.specValues[spec.id];
                            html += '<div class="form-group"><label class="control-label">'+spec.name+'</label>';
                            if( parseInt( spec.type_id ) == 3 ) {
                                html += '<div class="multiSelectBlock"><div class="controls">';
                                html += '<select class="form-control" name="SPEC['+spec.alias+'][]" multiple="10" style="height:150px;">';
                                for( j = 0; j < values.length; j++ ) {
                                    val = values[j];
                                    html += '<option value="'+val.alias+'">'+val.name+'</option>';
                                }
                                html += '</select>';
                                html += '</div></div>';
                            }
                            if( parseInt( spec.type_id ) == 2 || parseInt( spec.type_id ) == 1 ) {
                                html += '<div class=""><div class="controls">';
                                html += '<select class="form-control" name="SPEC['+spec.alias+']">';
                                html +='<option value="0"><?php echo __('Нет'); ?></option>';
                                for( j = 0; j < values.length; j++ ) {
                                    val = values[j];
                                    html += '<option value="'+val.alias+'">'+val.name+'</option>';
                                }
                                html += '</select>';
                                html += '</div></div>';
                            }
                            html += '</div>';
                        }
                    }
                    $('#specGroup').html(html);
                    multi_select();
                }
            });
        });

        $('#brand_alias').on('change', function(){
            var brand_alias = $(this).val();
            $.ajax({
                url: '/wezom/ajax/catalog/getModelsByBrandID',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    brand_alias: brand_alias
                },
                success: function(data){
                    var html = '<option value="0"><?php echo __('Нет'); ?></option>';
                    for(var i = 0; i < data.options.length; i++) {
                        html += '<option value="' + data.options[i].alias + '">' + data.options[i].name + '</option>';
                    }
                    $('#model_alias').html(html);
                }
            });
        });
    });
</script>