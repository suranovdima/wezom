<?php
use Core\HTML;
echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Основные данные'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                    </div>
                    <?php if($sertificate){ ?>
                        <div class="form-group">
                            <?php echo \Forms\Builder::input([
                                'name' => 'FORM[name]',
                                'value' => $obj->name,
                            ], __('Название')); ?>
                        </div>
                        <input type="hidden" name="FORM[type]" value="sertificate">
                    <?php }elseif($offer){ ?>
                        <div class="form-group">
                            <?php echo \Forms\Builder::input([
                                'name' => 'FORM[name]',
                                'value' => $obj->name,
                            ], __('Название')); ?>
                        </div>
                        <input type="hidden" name="FORM[type]" value="offer">
                    <?php }elseif($partner){ ?>
                        <div class="form-group">
                            <?php echo \Forms\Builder::input([
                                'name' => 'FORM[link]',
                                'value' => $obj->link,
                            ], __('Ссылка')); ?>
                        </div>
                        <input type="hidden" name="FORM[type]" value="partner">
                    <?php } ?>
                    <?php if($sertificate || $partner): ?>
                        <div class="form-group">
                            <label class="control-label"></label>
                            <div>
                                <?php if (is_file( HOST . Core\HTML::media('images/sliders/big/'.$obj->image, false) )): ?>
                                    <div class="contentImageView">
                                        <a href="<?php echo Core\HTML::media('images/sliders/big/'.$obj->image); ?>" class="mfpImage">
                                            <img src="<?php echo Core\HTML::media('images/sliders/small/'.$obj->image); ?>" />
                                        </a>
                                    </div>
                                    <div class="contentImageControl">
                                        <a class="btn btn-danger otherBtn" href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image/<?php echo $obj->id; ?>">
                                            <i class="fa fa-remove"></i>
                                            <?php echo __('Удалить изображение'); ?>
                                        </a>
                                        <br>
                                        <a class="btn btn-warning otherBtn" href="<?php echo \Core\General::crop('sliders', 'small', $obj->image); ?>">
                                            <i class="fa fa-pencil"></i>
                                            <?php echo __('Редактировать'); ?>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <?php echo \Forms\Builder::file(['name' => 'file']); ?>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if($sertificate): ?>
                    <div class="form-group">
                        <div class="form-group">
                            <label class="control-label"><?php echo __('Файл'); ?> (.pdf)</label>
                            <?php if ($obj->pdf != NULL): ?>
                               <a class="btn" href='<?=HTML::media('pdfs/'.$obj->pdf)?>' target='_blank'>Просмотреть загруженный файл</a><br><br>
                               <a class="btn" href='/wezom/sertificates/delete_file/<?=$obj->id?>'>Удалить файл</a><br>
                               <br><input type="hidden" name="FORM[pdf]" value="<?=$obj->pdf?>" />
                            <?php else: ?>
                                <input type="file" name="pdf" accept="application/pdf" />
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>