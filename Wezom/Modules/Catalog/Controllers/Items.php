<?php

namespace Wezom\Modules\Catalog\Controllers;

use Core\Config;
use Core\HTML;
use Core\Image\Image;
use Core\QB\DB;
use Core\Route;
use Core\Widgets;
use Core\Message;
use Core\Arr;
use Core\Support;
use Core\HTTP;
use Core\View;
use Core\Files;
use Core\Pager\Pager;
use Wezom\Modules\Catalog\Models\Items AS Model;
use Wezom\Modules\Catalog\Models\CatalogImages AS Images;
use Wezom\Modules\Catalog\Models\Brands;
use Wezom\Modules\Catalog\Models\Specifications;
use Wezom\Modules\Catalog\Models\SpecificationsValues;
use Wezom\Modules\Catalog\Models\Models;

class Items extends \Wezom\Modules\Base {

    public $tpl_folder = 'Catalog/Items';
    public $page;
    public $limit;
    public $offset;

    function before() {
        parent::before();
        $this->_seo['h1'] = __('Товары');
        $this->_seo['title'] = __('Товары');
        $this->setBreadcrumbs(__('Товары'), 'wezom/' . Route::controller() . '/index');
        $this->page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $this->limit = (int) Arr::get($_GET, 'limit', Config::get('basic.limit_backend')) < 1 ? : Arr::get($_GET, 'limit', Config::get('basic.limit_backend'));
        $this->offset = ($this->page - 1) * $this->limit;
    }

    function indexAction() {
        $status = NULL;
        if (isset($_GET['status']) && $_GET['status'] != '') {
            $status = Arr::get($_GET, 'status', 1);
        }
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $count = Model::countRows($status);
        $result = Model::getRows($status, 'sort', 'ASC', $this->limit, ($page - 1) * $this->limit);
        $pager = Pager::factory($page, $count, $this->limit)->create();
        $this->_toolbar = Widgets::get('Toolbar_List', ['add' => 1, 'delete' => 1]);
        $this->_content = View::tpl(
                        [
                    'result' => $result,
                    'tpl_folder' => $this->tpl_folder,
                    'tablename' => Model::$table,
                    'count' => $count,
                    'pager' => $pager,
                    'pageName' => $this->_seo['h1'],
                    'tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', Arr::get($_GET, 'parent_id')),
                        ], $this->tpl_folder . '/Index');
    }

    function editAction() {
        if ($_POST) {
            $post = $_POST['FORM'];
            // Set default settings for some fields
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['new'] = Arr::get($_POST, 'new', 0);
            $post['top'] = Arr::get($_POST, 'top', 0);
            $post['sale'] = Arr::get($_POST, 'sale', 0);
            $post['available'] = Arr::get($_POST, 'available', 0);
            $post['cost'] = (int) Arr::get($post, 'cost', 0);
            $post['cost_old'] = (int) Arr::get($post, 'cost_old', 0);
            $post['brand_alias'] = Arr::get($post, 'brand_alias') ? : NULL;
            $post['model_alias'] = Arr::get($post, 'model_alias') ? : NULL;
            $post['sort'] = (int) Arr::get($post, 'sort');
            if(!isset($post['pdf'])){
                $resultat = Files::uploadFile('/Media/pdfs','pdf');
                if($resultat) {
                    $post['pdf'] = $resultat;
                    $post['size_of_pdf'] = Files::formatSizeUnits($_FILES['pdf']['size']);
                }
            }
            $post['min_price'] = min([(int)$post['cost'], (int)$post['cost1'], (int)$post['cost2']]);
            // Check form for rude errors
            if (Model::valid($post)) {
                $post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'), Route::param('id'));
                $res = Model::update($post, Route::param('id'));
                if ($res) {
                    Message::GetMessage(1, __('Вы успешно изменили данные!'));
                } else {
                    Message::GetMessage(0, __('Не удалось изменить данные!'));
                }
                $this->redirectAfterSave(Route::param('id'));
            }
            $result = Arr::to_object($post);
        } else {
            $result = Model::getRow(Route::param('id'));
//            $specArray = Model::getItemSpecificationsAliases(Route::param('id'));
        }
        $this->_toolbar = Widgets::get('Toolbar_Edit');
        $this->_seo['h1'] = __('Редактирование');
        $this->_seo['title'] = __('Редактирование');
        $this->setBreadcrumbs(__('Редактирование'), 'wezom/' . Route::controller() . '/edit/' . Route::param('id'));
        $rel = DB::select('catalog.*', 'catalog_images.image')
                ->from('catalog')
                ->join('catalog_images', 'LEFT')->on('catalog.id', '=', 'catalog_images.catalog_id')->on('catalog_images.main', '=', DB::expr('1'))
                ->find_all();
        $this->_content = View::tpl(
                        [
                    'obj' => $result,
                    'tpl_folder' => $this->tpl_folder,
                    'tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', $result->parent_id),
                    'uploader' => View::tpl([], $this->tpl_folder . '/Upload'),
                    'related' => View::tpl(['tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree'), 'itemID' => Route::param('id'), 'items' => $rel], $this->tpl_folder . '/Related'),
                        ], $this->tpl_folder . '/Form');
    }

    function addAction() {
        if ($_POST) {
            $post = $_POST['FORM'];
            // Set default settings for some fields
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['new'] = Arr::get($_POST, 'new', 0);
            $post['top'] = Arr::get($_POST, 'top', 0);
            $post['sale'] = Arr::get($_POST, 'sale', 0);
            $post['available'] = Arr::get($_POST, 'available', 0);
            $post['cost'] = (int) Arr::get($post, 'cost', 0);
            $post['cost_old'] = (int) Arr::get($post, 'cost_old', 0);
            $post['brand_alias'] = Arr::get($post, 'brand_alias') ? : NULL;
            $post['model_alias'] = Arr::get($post, 'model_alias') ? : NULL;
            $post['sort'] = (int) Arr::get($post, 'sort');
            if(!isset($post['pdf'])){
                $resultat = Files::uploadFile('/Media/pdfs','pdf');
                if($resultat) {
                    $post['pdf'] = $resultat;
                    $post['size_of_pdf'] = Files::formatSizeUnits($_FILES['pdf']['size']);
                }
            }
            $post['min_price'] = min([(int)$post['cost'], (int)$post['cost1'], (int)$post['cost2']]);
            // Check form for rude errors
            if (Model::valid($post)) {
                $post['alias'] = Model::getUniqueAlias(Arr::get($post, 'alias'));
                $res = Model::insert($post);
                if ($res) {
                    Message::GetMessage(1, __('Вы успешно добавили данные!'));
					$this->redirectAfterSave($res);
                } else {
                    Message::GetMessage(0, __('Не удалось добавить данные!'));
                }
            }
            $result = Arr::to_object($post);
            $parent_id = $result->parent_id;
        } else {
            $result = [];
            $models = [];
            $parent_id = 0;
        }
        $this->_toolbar = Widgets::get('Toolbar_Edit');
        $this->_seo['h1'] = __('Добавление');
        $this->_seo['title'] = __('Добавление');
        $this->setBreadcrumbs(__('Добавление'), 'wezom/' . Route::controller() . '/add');

        $this->_content = View::tpl(
                        [
                    'obj' => $result,
                    'tpl_folder' => $this->tpl_folder,
                    'tree' => Support::getSelectOptions('Catalog/Items/Select', 'catalog_tree', $parent_id),
                    'uploader' => NULL,
                    'related' => NULL,
                        ], $this->tpl_folder . '/Form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        $page = Model::getRow($id);
        if (!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/' . Route::controller() . '/index');
        }
        $images = Images::getRows($id);
        foreach ($images AS $im) {
            Images::deleteImage($im->image);
        }
        Model::delete($id);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/' . Route::controller() . '/index');
    }

    function deleteFilePdfAction() {
        $id = (int) Route::param('id');
        $page = Model::getRow($id);
        if (!$page) {
            Message::GetMessage(0, __('Данные не существуют!'));
            HTTP::redirect('wezom/' . Route::controller() . '/index');
        }
        Model::deletePdf($page->pdf, $id, 'pdf', true);
        Message::GetMessage(1, __('Данные удалены!'));
        HTTP::redirect('wezom/' . Route::controller() . '/edit/' . $id);
    }
}
