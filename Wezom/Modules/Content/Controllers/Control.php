<?php
    namespace Wezom\Modules\Content\Controllers;

    use Core\Route;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;
    use Core\Files;

    use Wezom\Modules\Content\Models\Control AS Model;

    class Control extends \Wezom\Modules\Base {

        public $tpl_folder = 'Content/Control';

        function before() {
            parent::before();
            $this->_seo['h1'] = __('Системные страницы');
            $this->_seo['title'] = __('Системные страницы');
            $this->setBreadcrumbs(__('Системные страницы'), 'wezom/'.Route::controller().'/index');
        }

        function indexAction () {
            $result = Model::getRows(NULL, 'name', 'ASC');
            $this->_content = View::tpl(
                [
                    'result'        => $result,
                    'tpl_folder'    => $this->tpl_folder,
                    'tablename'     => Model::$table,
                ], $this->tpl_folder.'/Index');
        }

        function editAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                if(!isset($post['pdf'])){
                    $resultat = Files::uploadFile('/Media/pdfs','pdf');
                    if($resultat) {
                        $post['pdf'] = $resultat;
                        $post['size_of_pdf'] = Files::formatSizeUnits($_FILES['pdf']['size']);
                    }
                }
                if(!isset($post['xls'])){
                    $resultat = Files::uploadFile('/Media/xls','xls');
                    if($resultat) {
                        $post['xls'] = $resultat;
                        $post['size_of_xls'] = Files::formatSizeUnits($_FILES['xls']['size']);
                    }
                }
                if(isset($_POST['longitude']) || isset($_POST['latitude'])) {
                    $post['other'] = json_encode(array(
                        'longitude' => Arr::get($_POST, 'longitude'),
                        'latitude' => Arr::get($_POST, 'latitude'),
                        'zoom' => Arr::get($_POST, 'zoom'),
                    ));
                }
                if( Model::valid($post) ) {
                    $res = Model::update($post, Route::param('id'));
                    if($res) {
                        Model::uploadImage(Route::param('id'));
                        Message::GetMessage(1, __('Вы успешно изменили данные!'));
                    } else {
                        Message::GetMessage(0, __('Не удалось изменить данные!'));
                    }
                    $this->redirectAfterSave(Route::param('id'));
                }
                $result = Arr::to_object($post);
            } else {
                $result = Model::getRow(Route::param('id'));
            }
            $this->_toolbar = Widgets::get('Toolbar/Edit', ['noAdd' => true]);
            $this->_seo['h1'] = __('Редактирование');
            $this->_seo['title'] = __('Редактирование');
            $this->setBreadcrumbs(__('Редактирование'), 'wezom/'.Route::controller().'/index');
            $this->_content = View::tpl(
                [
                    'obj' => $result,
                    'tpl_folder' => $this->tpl_folder,
                ], $this->tpl_folder.'/Form');
        }

        function deleteImageAction() {
            $id = (int) Route::param('id');
            $page = Model::getRow($id);
            if(!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                HTTP::redirect('wezom/'.Route::controller().'/index');
            }
            Model::deleteImage($page->image, $id);
            Message::GetMessage(1, __('Данные удалены!'));
            HTTP::redirect('wezom/'.Route::controller().'/edit/'.$id);
        }

        function deleteFilePdfAction() {
            $id = (int) Route::param('id');
            $page = Model::getRow($id);
            if (!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                HTTP::redirect('wezom/' . Route::controller() . '/index');
            }
            Model::deletePdf($page->pdf, $id, 'pdf', true);
            Message::GetMessage(1, __('Данные удалены!'));
            HTTP::redirect('wezom/' . Route::controller() . '/edit/' . $id);
        }

        function deleteFileXlsAction() {
            $id = (int) Route::param('id');
            $page = Model::getRow($id);
            if (!$page) {
                Message::GetMessage(0, __('Данные не существуют!'));
                HTTP::redirect('wezom/' . Route::controller() . '/index');
            }
            Model::deleteXls($page->xls, $id, 'xls', true);
            Message::GetMessage(1, __('Данные удалены!'));
            HTTP::redirect('wezom/' . Route::controller() . '/edit/' . $id);
        }
    }