<?php
    namespace Wezom\Modules\Content\Models;

    use Core\Arr;
    use Core\Message;

    class Control extends \Core\Common {

        public static $table = 'control';
        public static $image = 'control';
        public static $pdf = 'control';
        public static $xls = 'control';

    }