<?php   
    
    return [
        // System pages
        'wezom/control/index' => 'content/control/index',
        'wezom/control/index/page/<page:[0-9]*>' => 'content/control/index',
        'wezom/control/delete_image/<id:[0-9]*>' => 'content/control/deleteImage',
        'wezom/control/delete_file_pdf/<id:[0-9]*>' => 'content/control/deleteFilePdf',
        'wezom/control/delete_file_xls/<id:[0-9]*>' => 'content/control/deleteFileXls',
        'wezom/control/edit/<id:[0-9]*>' => 'content/control/edit',
        // Content
        'wezom/content/index' => 'content/content/index',
        'wezom/content/index/page/<page:[0-9]*>' => 'content/content/index',
        'wezom/content/edit/<id:[0-9]*>' => 'content/content/edit',
        'wezom/content/delete/<id:[0-9]*>' => 'content/content/delete',
        'wezom/content/add' => 'content/content/add',
    ];