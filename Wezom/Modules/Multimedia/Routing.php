<?php

    return [
        // Banners
        'wezom/banners/index' => 'multimedia/banners/index',
        'wezom/banners/index/page/<page:[0-9]*>' => 'multimedia/banners/index',
        'wezom/banners/edit/<id:[0-9]*>' => 'multimedia/banners/edit',
        'wezom/banners/delete/<id:[0-9]*>' => 'multimedia/banners/delete',
        'wezom/banners/delete_image/<id:[0-9]*>' => 'multimedia/banners/deleteImage',
        'wezom/banners/add' => 'multimedia/banners/add',
        // Gallery
        'wezom/gallery' => 'multimedia/gallery/index',
        'wezom/gallery/index' => 'multimedia/gallery/index',
        'wezom/gallery/index/page/<page:[0-9]*>' => 'multimedia/gallery/index',
        'wezom/gallery/edit/<id:[0-9]*>' => 'multimedia/gallery/edit',
        'wezom/gallery/delete/<id:[0-9]*>' => 'multimedia/gallery/delete',
        'wezom/gallery/delete_image/<id:[0-9]*>' => 'multimedia/gallery/deleteImage',
        'wezom/gallery/add' => 'multimedia/gallery/add',
    ];
