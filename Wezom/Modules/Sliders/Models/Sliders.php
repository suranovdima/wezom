<?php
namespace Wezom\Modules\Sliders\Models;

use Core\Arr;
use Core\Message;

class Sliders extends \Core\Common {

	public static $table = 'sliders';
	public static $image = 'sliders';
	public static $pdf = 'sliders';
	public static $rules = [];

	public static function valid($data = [])
	{
		static::$rules = [
			[
				'error' => __('Название не может быть пустым!'),
				'key' => 'not_empty',
			],
		];
		return parent::valid($data);
	}

}