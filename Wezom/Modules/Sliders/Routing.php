<?php

    return [
        //Sertificates
        'wezom/sertificates/index' => 'sliders/sertificates/index',
        'wezom/sertificates/edit/<id:[0-9]*>' => 'sliders/sertificates/edit',
        'wezom/sertificates/delete/<id:[0-9]*>' => 'sliders/sertificates/delete',
        'wezom/sertificates/delete_image/<id:[0-9]*>' => 'sliders/sertificates/deleteImage',
        'wezom/sertificates/delete_file/<id:[0-9]*>' => 'sliders/sertificates/deleteFile',
        'wezom/sertificates/add' => 'sliders/sertificates/add',
        //Partners
        'wezom/partners/index' => 'sliders/partners/index',
        'wezom/partners/edit/<id:[0-9]*>' => 'sliders/partners/edit',
        'wezom/partners/delete/<id:[0-9]*>' => 'sliders/partners/delete',
        'wezom/partners/delete_image/<id:[0-9]*>' => 'sliders/partners/deleteImage',
        'wezom/partners/add' => 'sliders/partners/add',
        //Offers
        'wezom/offers/index' => 'sliders/offers/index',
        'wezom/offers/edit/<id:[0-9]*>' => 'sliders/offers/edit',
        'wezom/offers/delete/<id:[0-9]*>' => 'sliders/offers/delete',
        'wezom/offers/add' => 'sliders/offers/add',
    ];
