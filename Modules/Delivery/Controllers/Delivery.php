<?php
namespace Modules\Delivery\Controllers;

use Core\Route;
use Core\Config;
use Core\HTML;
use Core\Widgets;
use Modules\Base;
use Core\View;
use Modules\Content\Models\Control;

class Delivery extends Base
{
	public $current;

	public function before()
	{
		parent::before();
		$this->current = Control::getRow('delivery', 'alias', 1);
		if (!$this->current) {
			return Config::error();
		}
	}

	public function indexAction()
	{
		// Check for existance
		$this->_template = 'Content/Control';
		// Check for existance
		if (Config::get('error')) {
			return false;
		}
		// Seo
		$this->_seo['h1'] = $this->current->h1;
		$this->_seo['title'] = $this->current->title;
		$this->_seo['keywords'] = $this->current->keywords;
		$this->_seo['description'] = $this->current->description;
		// Render template
		$this->_content = View::tpl(['current' => $this->current], 'Content/Delivery/Delivery');
	}

}
    