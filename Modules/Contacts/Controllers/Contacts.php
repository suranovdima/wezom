<?php
namespace Modules\Contacts\Controllers;

use Core\Route;
use Core\Config;
use Core\HTML;
use Core\Widgets;
use Modules\Base;
use Core\View;
use Modules\Content\Models\Control;

class Contacts extends Base
{
	public $current;

	public function before()
	{
		parent::before();
		$this->current = Control::getRow('contacts', 'alias', 1);
		if (!$this->current) {
			return Config::error();
		}
	}

	public function indexAction()
	{
		// Check for existance
		$this->_template = 'Content/Control';
		// Check for existance
		if (Config::get('error')) {
			return false;
		}
		// Seo
		$this->_seo['h1'] = $this->current->h1;
		$this->_seo['title'] = $this->current->title;
		$this->_seo['keywords'] = $this->current->keywords;
		$this->_seo['description'] = $this->current->description;
		$offers = Control::getRowsFromTableByField('sliders', 'offer', 'type', 1);
		$this->offers = $offers;
		// Render template
		$this->_content = View::tpl(['offers' => $offers, 'current' => $this->current], 'Content/Contacts/Contacts');
	}

}
    