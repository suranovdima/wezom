<?php
namespace Modules\Content\Models;

use Core\Common;
use Core\QB\DB;

class Control extends Common
{

    public static $table = 'control';
	
	public static function getForSitemapXml() {
			
		$result = DB::select()
					->from(static::$table)
					->where('status', '=', 1)
					->where('sitemapxml', '=', 1)
					->order_by('id', 'ASC')
					->find_all();

		return $result;

	}

}