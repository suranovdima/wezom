<?php
namespace Modules\Price\Controllers;

use Core\Route;
use Core\Config;
use Core\HTML;
use Core\Widgets;
use Modules\Base;
use Core\Common;
use Core\View;
use Modules\Content\Models\Control;

class Price extends Base
{
	public $current;

	public function before()
	{
		parent::before();
		$this->current = Control::getRow('price', 'alias', 1);
		if (!$this->current) {
			return Config::error();
		}
	}

	public function indexAction()
	{
		// Check for existance
		$this->_template = 'Content/Control';
		// Check for existance
		if (Config::get('error')) {
			return false;
		}
		// Seo
		$this->_seo['h1'] = $this->current->h1;
		$this->_seo['title'] = $this->current->title;
		$this->_seo['keywords'] = $this->current->keywords;
		$this->_seo['description'] = $this->current->description;
		$products = Common::factory('catalog')->getRows(1, 'sort', 'ASC');
		// Render template
		$this->_content = View::tpl(['products' => $products, 'current' => $this->current], 'Content/Price/Price');
	}

}
    