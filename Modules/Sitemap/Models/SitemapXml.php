<?php
namespace Modules\Sitemap\Models;

use Core\HTML;

class SitemapXml
{

    
	public static function createIndexSitemap($pathes, $scheme) {
		
		$dom = new \domDocument('1.0', 'utf-8');
        $root = $dom->createElement('sitemapindex');
		$root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		
		foreach ($pathes as $path) {
			$root->appendchild(self::createSitemapElement($path, $dom, $scheme));
		}
		
		$dom->formatOutput = true;
		 $dom->appendChild($root);
        $dom->save(HOST.'/sitemap.xml');
		return true;
		
	}
	
	public static function createSitemapElement($path, $dom, $scheme = null, $lastModification = null) {
		
		$sitemap = $dom->createElement('sitemap');
		$loc = $dom->createElement('loc', HTML::link($path, true, $scheme));
		$sitemap->appendChild($loc);
		if ($lastModification !== null) {
			$lastmod = $dom->createElement('lastmod', date('Y-m-d\TH:i:sP', $lastModification));
			$sitemap->appendChild($loc);
		}
		
		return $sitemap;
		
	}
	
	public static function createUrlElement($link, $dom, $scheme = null, $lastModification = null) {
		
		$url = $dom->createElement('url');
		$loc = $dom->createElement('loc', HTML::link($link, true, $scheme));
		$url->appendChild($loc);
		if ($lastModification !== null) {
			$lastmod = $dom->createElement('lastmod', date('Y-m-d\TH:i:sP', $lastModification));
			$url->appendChild($loc);
		}
		
		return $url;
	}
    
	
}