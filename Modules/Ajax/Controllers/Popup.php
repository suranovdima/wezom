<?php
namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\QB\DB;
use Core\User;
use Core\Widgets;
use Core\Config;
use Core\Common;
use Modules\Catalog\Models\Items;


class Popup extends \Modules\Ajax {

	public function callbackAction() {
		echo Widgets::get('Popup/Callback');
	}

	public function after() {
		die;
	}
}