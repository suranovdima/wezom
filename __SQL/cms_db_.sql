-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Час створення: Лис 14 2017 р., 12:24
-- Версія сервера: 5.7.13-log
-- Версія PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `cms_db`
--

-- --------------------------------------------------------

--
-- Структура таблиці `access`
--

DROP TABLE IF EXISTS `access`;
CREATE TABLE IF NOT EXISTS `access` (
  `id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `controller` varchar(64) DEFAULT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `access`
--

INSERT INTO `access` (`id`, `role_id`, `controller`, `view`, `edit`) VALUES
(2, 5, 'index', 1, 0),
(3, 5, 'content', 0, 0),
(4, 5, 'control', 0, 0),
(5, 5, 'news', 0, 0),
(6, 5, 'articles', 1, 0),
(7, 5, 'menu', 0, 0),
(8, 5, 'slider', 0, 0),
(9, 5, 'gallery', 0, 0),
(10, 5, 'banners', 0, 0),
(11, 5, 'questions', 0, 0),
(12, 5, 'comments', 0, 0),
(13, 5, 'groups', 0, 0),
(14, 5, 'items', 0, 0),
(15, 5, 'brands', 0, 0),
(16, 5, 'models', 0, 0),
(17, 5, 'specifications', 0, 0),
(18, 5, 'orders', 0, 0),
(19, 5, 'simple', 0, 0),
(20, 5, 'users', 0, 0),
(21, 5, 'subscribers', 0, 0),
(22, 5, 'subscribe', 0, 0),
(23, 5, 'contacts', 0, 0),
(24, 5, 'callback', 0, 0),
(25, 5, 'mailTemplates', 0, 0),
(26, 5, 'config', 0, 0),
(27, 5, 'templates', 0, 0),
(28, 5, 'links', 0, 0),
(29, 5, 'scripts', 0, 0),
(30, 5, 'redirects', 0, 0),
(31, 6, 'button', 0, 0),
(32, 6, 'index', 0, 0),
(33, 6, 'content', 0, 0),
(34, 6, 'control', 0, 0),
(35, 6, 'news', 0, 0),
(36, 6, 'articles', 0, 0),
(37, 6, 'menu', 0, 0),
(38, 6, 'slider', 0, 0),
(39, 6, 'gallery', 0, 0),
(40, 6, 'banners', 0, 0),
(41, 6, 'questions', 0, 0),
(42, 6, 'comments', 0, 0),
(43, 6, 'groups', 0, 0),
(44, 6, 'items', 0, 0),
(45, 6, 'brands', 0, 0),
(46, 6, 'models', 0, 0),
(47, 6, 'specifications', 0, 0),
(48, 6, 'orders', 0, 0),
(49, 6, 'simple', 0, 0),
(50, 6, 'users', 0, 0),
(51, 6, 'subscribers', 0, 0),
(52, 6, 'subscribe', 0, 0),
(53, 6, 'contacts', 0, 0),
(54, 6, 'callback', 0, 0),
(55, 6, 'mailTemplates', 0, 0),
(56, 6, 'config', 0, 0),
(57, 6, 'templates', 0, 0),
(58, 6, 'links', 0, 0),
(59, 6, 'scripts', 0, 0),
(60, 6, 'redirects', 0, 0),
(121, 7, 'button', 0, 0),
(122, 7, 'index', 1, 0),
(123, 7, 'content', 1, 1),
(124, 7, 'control', 1, 1),
(125, 7, 'news', 1, 1),
(126, 7, 'articles', 1, 1),
(127, 7, 'menu', 1, 1),
(128, 7, 'slider', 1, 1),
(129, 7, 'gallery', 1, 1),
(130, 7, 'banners', 1, 1),
(131, 7, 'questions', 1, 1),
(132, 7, 'comments', 1, 1),
(133, 7, 'groups', 1, 1),
(134, 7, 'items', 1, 1),
(135, 7, 'brands', 1, 1),
(136, 7, 'models', 1, 1),
(137, 7, 'specifications', 1, 1),
(138, 7, 'orders', 1, 1),
(139, 7, 'simple', 1, 1),
(140, 7, 'users', 1, 1),
(141, 7, 'subscribers', 1, 1),
(142, 7, 'subscribe', 1, 1),
(143, 7, 'contacts', 1, 1),
(144, 7, 'callback', 1, 1),
(145, 7, 'mailTemplates', 1, 1),
(146, 7, 'config', 1, 1),
(147, 7, 'templates', 1, 1),
(148, 7, 'links', 1, 1),
(149, 7, 'scripts', 1, 1),
(150, 7, 'redirects', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `text` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `banners`
--

INSERT INTO `banners` (`id`, `created_at`, `updated_at`, `status`, `text`, `url`, `image`) VALUES
(8, 1447094167, 1510586460, 1, 'Логотип', '/', '00ddf19741ac3d7b00f8c6dc5d05fd91.png'),
(9, 1510244748, NULL, 1, 'Картинка для виджета "Оставить заявку"', '#', '4d61a862085c23b3c7a1485aa24cfc2d.png'),
(10, 1510586443, NULL, 1, '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE IF NOT EXISTS `blacklist` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `callback`
--

DROP TABLE IF EXISTS `callback`;
CREATE TABLE IF NOT EXISTS `callback` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `callback`
--

INSERT INTO `callback` (`id`, `created_at`, `updated_at`, `status`, `name`, `phone`, `ip`) VALUES
(1, 1510588782, NULL, 0, 'dsadasdas', '+38(014)124-12-41', '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблиці `catalog`
--

DROP TABLE IF EXISTS `catalog`;
CREATE TABLE IF NOT EXISTS `catalog` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `parent_id` int(10) DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `cost` int(6) NOT NULL DEFAULT '0',
  `cost1` varchar(255) NOT NULL,
  `cost2` varchar(255) NOT NULL,
  `min_price` int(11) NOT NULL DEFAULT '0',
  `cost_old` int(6) NOT NULL DEFAULT '0',
  `artikul` varchar(128) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0',
  `brand_alias` varchar(255) DEFAULT NULL,
  `model_alias` varchar(255) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `specifications` text,
  `product_description` text,
  `pdf` varchar(255) DEFAULT NULL,
  `size_of_pdf` text
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `catalog`
--

INSERT INTO `catalog` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `alias`, `parent_id`, `new`, `sale`, `top`, `available`, `h1`, `title`, `keywords`, `description`, `cost`, `cost1`, `cost2`, `min_price`, `cost_old`, `artikul`, `views`, `brand_alias`, `model_alias`, `image`, `specifications`, `product_description`, `pdf`, `size_of_pdf`) VALUES
(164, 1510146877, 1510564281, 1, 1, 'Плиты пенополистирольные 1', 'plity-penopolistirolnye-1', 55, 0, 0, 0, 0, '', '', '', '', 180, '156', '144', 144, 0, 'Продукция', 0, NULL, NULL, '7124c09e7c799be795275ecfa903e90b.jpg', NULL, 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', 'f145cd6e06bd4be12ab7904f81e5d760.pdf', '7.76 KB'),
(165, 1510146923, 1510585728, 1, 0, 'Плиты пенополистирольные 2', 'plity-penopolistirolnye-2', 55, 0, 0, 0, 0, '', '', '', '', 180, '155', '142', 142, 0, 'Продукция', 0, NULL, NULL, 'a238d02e537cc055c2aa1b3321ac3b06.jpg', NULL, 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', 'f4b00aafed6e448654bcd0c1cb7bf55e.pdf', '7.76 KB'),
(166, 1510146938, 1510564257, 1, 0, 'Плиты пенополистирольные 3', 'plity-penopolistirolnye-3', 55, 0, 0, 0, 0, '', '', '', '', 180, '177', '175', 175, 0, 'Продукция', 0, NULL, NULL, 'd64f41de406cfc3060abad16eab828ac.jpg', NULL, 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', 'b0c7287a96b0d10ca1a8a241385bffa0.pdf', '330.30 KB'),
(167, 1510146962, 1510564242, 1, 0, 'Плиты пенополистирольные 4', 'plity-penopolistirolnye-4', 55, 0, 0, 0, 0, '', '', '', '', 180, '175', '185', 175, 0, 'Продукция', 0, NULL, NULL, 'c89addf54d8f50bbfc4990c5cea64acf.jpg', NULL, 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', 'da07aa9cf032a7af007deb9e89a21b8c.pdf', '330.30 KB'),
(168, 1510146988, 1510586330, 1, 0, 'Плиты пенополистирольные 5', 'plity-penopolistirolnye-5', 55, 0, 0, 0, 0, '', '', '', '', 155, '140', '130', 130, 0, 'Продукция', 2, NULL, NULL, '45fbeb304dadfae683cb66d4803178c5.jpg', NULL, 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', 'd7a8956e24aeb67850a58af5bea9ccae.pdf', '7.76 KB');

-- --------------------------------------------------------

--
-- Структура таблиці `catalog_images`
--

DROP TABLE IF EXISTS `catalog_images`;
CREATE TABLE IF NOT EXISTS `catalog_images` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(10) NOT NULL DEFAULT '0',
  `catalog_id` int(10) NOT NULL DEFAULT '0',
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1138 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `catalog_images`
--

INSERT INTO `catalog_images` (`id`, `created_at`, `updated_at`, `sort`, `catalog_id`, `main`, `image`) VALUES
(1121, 1510310440, 1510586330, 0, 168, 1, '45fbeb304dadfae683cb66d4803178c5.jpg'),
(1122, 1510310456, NULL, 0, 167, 1, 'c89addf54d8f50bbfc4990c5cea64acf.jpg'),
(1123, 1510310475, NULL, 0, 166, 1, 'd64f41de406cfc3060abad16eab828ac.jpg'),
(1124, 1510310492, NULL, 0, 165, 1, 'a238d02e537cc055c2aa1b3321ac3b06.jpg'),
(1125, 1510310510, NULL, 0, 164, 1, '7124c09e7c799be795275ecfa903e90b.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `catalog_tree`
--

DROP TABLE IF EXISTS `catalog_tree`;
CREATE TABLE IF NOT EXISTS `catalog_tree` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `image` varchar(64) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `views` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `catalog_tree`
--

INSERT INTO `catalog_tree` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `alias`, `parent_id`, `image`, `h1`, `title`, `keywords`, `description`, `text`, `views`) VALUES
(55, 1510146863, 1510560299, 1, 0, 'Плиты пенополистирольные', 'products', 0, NULL, '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `zna` text,
  `updated_at` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(32) DEFAULT NULL,
  `values` text COMMENT 'Возможные значения в json массиве ключ => значение. Для селекта и радио',
  `group` varchar(128) DEFAULT NULL COMMENT 'Группа настроек'
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `config`
--

INSERT INTO `config` (`id`, `name`, `zna`, `updated_at`, `status`, `sort`, `key`, `valid`, `type`, `values`, `group`) VALUES
(1, 'E-Mail администратора сайта (отправитель по умолчанию)', 'suranov.d.wezom@gmail.com', 1434885560, 1, 1, 'admin_email', 1, 'input', NULL, 'mail'),
(2, 'Название сайта', 'CMS v4.0', 1434885560, 1, 1, 'name_site', 1, 'input', NULL, 'basic'),
(4, 'Copyright', 'Copyright © 2017. Полимер Славутич, ООО', 1434885560, 1, 4, 'copyright', 1, 'input', NULL, 'static'),
(5, 'Отображение всплывающих сообщений на сайте', 'topRight', 1434835221, 0, 2000, 'message_output', 1, 'radio', '[{"key":"Отображать вверху","value":"top"},{"key":"Отображать вверху слева","value":"topLeft"},{"key":"Отображать вверху по центру","value":"topCenter"},{"key":"Отображать вверху справа","value":"topRight"},{"key":"Отображать по центру слева","value":"centerLeft"},{"key":"Отображать по центру","value":"center"},{"key":"Отображать по центру справа","value":"centerRight"},{"key":"Отображать внизу слева","value":"bottomLeft"},{"key":"Отображать внизу по центру","value":"bottomCenter"},{"key":"Отображать внизу справа","value":"bottomRight"},{"key":"Отображать внизу","value":"bottom"}]', 'basic'),
(6, 'Номер телефона в шапке сайта', '+38(050)552-88-00', 1434885560, 1, 6, 'phone', 0, 'input', NULL, 'static'),
(7, 'Количество товаров на странице', '15', 1434885560, 0, 7, 'limit', 1, 'input', NULL, 'basic'),
(8, 'Количество статей на главной странице', '1', 1434885560, 0, 8, 'limit_articles_main_page', 1, 'input', NULL, 'basic'),
(9, 'Количество строк в админ-панели', '10', 1434885560, 1, 9, 'limit_backend', 1, 'input', NULL, 'basic'),
(10, 'VK.com', 'http://vk.com/airpacgroup', 1434885560, 1, 10, 'vk', 0, 'input', NULL, 'socials'),
(11, 'FB.com', 'https://www.facebook.com', 1434885560, 1, 11, 'fb', 0, 'input', NULL, 'socials'),
(12, 'Instagram', 'http://instagram.com', 1434885560, 1, 12, 'instagram', 0, 'input', NULL, 'socials'),
(13, 'Слоган в подвале', 'Плиты и блоки пенополистирольные от производителя', 1434885560, 1, 5, 'footer_text', 0, 'input', NULL, 'static'),
(17, 'Количество новостей / статей на странице', '1', 1434885560, 0, 7, 'limit_articles', 1, 'input', NULL, 'basic'),
(18, 'Количество групп товаров на странице', '5', 1434885560, 0, 7, 'limit_groups', 1, 'input', NULL, 'basic'),
(19, 'Использовать СМТП', '1', 1434885560, 1, 3, 'smtp', 1, 'radio', '[{"key":"Да","value":1},{"key":"Нет","value":0}]', 'mail'),
(20, 'SMTP server', '', 1434885560, 1, 4, 'host', 0, 'input', NULL, 'mail'),
(22, 'Логин', '1111', 1434885560, 1, 5, 'username', 0, 'input', NULL, 'mail'),
(23, 'Пароль', '1111', 1434885560, 1, 6, 'pass', 0, 'password', NULL, 'mail'),
(24, 'Тип подключения', '', 1434885560, 1, 7, 'secure', 0, 'select', '[{"key":"TLS","value":"tls"},{"key":"SSL","value":"ssl"},{"key":"Отсутствует","value":""}]', 'mail'),
(25, 'Порт. Например 465 или 587. (587 по умолчанию)', '587', 1434885560, 1, 8, 'port', 0, 'input', NULL, 'mail'),
(26, 'Имя латинницей (отображается в заголовке письма)', 'Info', 1434885560, 1, 2, 'name', 1, 'input', NULL, 'mail'),
(27, 'Запаролить сайт', '1', 1434885560, 1, 0, 'auth', 1, 'radio', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 'security'),
(28, 'Логин', '1', 1434885560, 1, 2, 'username', 0, 'input', NULL, 'security'),
(29, 'Пароль', '1', 1434885560, 1, 3, 'password', 0, 'password', NULL, 'security'),
(30, 'Сократить CSS u JavaScript', '0', 1434885561, 1, 1, 'minify', 1, 'radio', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 'speed'),
(31, 'Сократить HTML', '0', 1434885561, 1, 2, 'compress', 1, 'radio', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 'speed'),
(32, 'Кеширование размера изображений', '0', NULL, 1, 3, 'cache_images', 1, 'select', '[{"key":"Выключить","value":"0"},{"key":"12 часов","value":"0.5"},{"key":"День","value":"1"},{"key":"3 дня","value":"3"},{"key":"Неделя","value":"7"},{"key":"2 недели","value":"14"},{"key":"Месяц","value":"30"},{"key":"Год","value":"365"}]', 'speed'),
(33, 'Наименование организации', 'wezom', NULL, 1, 1, 'organization', 1, 'input', NULL, 'microdata'),
(34, 'Автор статей', 'wezom author', NULL, 1, 2, 'author', 1, 'input ', NULL, 'microdata'),
(35, 'Количество отзывов на странице', '10', NULL, 0, 8, 'limit_reviews', 1, 'input', NULL, 'basic'),
(36, 'Наименование магазина', 'cms4', NULL, 1, 1, 'name', 1, 'input', NULL, 'export'),
(37, 'Наименование компании', 'wezom', NULL, 1, 2, 'company', 1, 'input', NULL, 'export'),
(38, 'Email разработчиков CMS или агентства, осуществляющего техподдержку.', 'suranov.d.wezom@gmail.com', NULL, 1, 3, 'email', 1, 'input', NULL, 'export'),
(40, 'Использовать защищённое ssl соединение?', '0', NULL, 1, 5, 'ssl', 1, 'radio', '[{"key":"Да","value":"1"},{"key":"Нет","value":"0"}]', 'security'),
(41, 'Ключ googlemaps', 'AIzaSyDGMr7U3w0bDthcw1jqIQRLbz-zXJmd0K4', 1505459047, 0, 100, 'googlemapskey', 0, 'input', NULL, 'basic'),
(42, 'Тип сайта', 'Test Wezom', NULL, 1, 3, 'site_type', 1, 'input', NULL, 'basic'),
(43, 'Адрес в шапке сайта', 'г. Славутич, Черниговский кв, д. 351', NULL, 1, 6, 'address', 1, 'input', NULL, 'static'),
(44, 'Телефон Дирекции', '+38(045)792-97-11', NULL, 1, 7, 'phone_ceo', 1, 'input', NULL, 'static'),
(45, 'Адрес в подвале сайта', 'Украина, Киевская область, Славутич, 07100, Черниговский квартал, д. 3511', NULL, 1, 6, 'address_footer', 1, 'input', NULL, 'static'),
(47, 'Ссылка на сайт в подвале', 'http://wezom.com.ua/', NULL, 1, 8, 'footer_site_link', 1, 'input', NULL, 'static');

-- --------------------------------------------------------

--
-- Структура таблиці `config_groups`
--

DROP TABLE IF EXISTS `config_groups`;
CREATE TABLE IF NOT EXISTS `config_groups` (
  `id` int(4) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `side` varchar(16) NOT NULL DEFAULT 'left' COMMENT 'left, right',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `config_groups`
--

INSERT INTO `config_groups` (`id`, `name`, `alias`, `side`, `status`, `sort`) VALUES
(1, 'Почта', 'mail', 'right', 1, 1),
(2, 'Базовые', 'basic', 'left', 1, 1),
(3, 'Статика', 'static', 'left', 1, 2),
(4, 'Соц. сети', 'socials', 'left', 0, 3),
(5, 'Безопасность', 'security', 'right', 1, 2),
(6, 'Быстродействие', 'speed', 'right', 1, 3),
(7, 'Микроразметка', 'microdata', 'left', 0, 4),
(8, 'Выгрузки', 'export', 'left', 0, 5);

-- --------------------------------------------------------

--
-- Структура таблиці `config_types`
--

DROP TABLE IF EXISTS `config_types`;
CREATE TABLE IF NOT EXISTS `config_types` (
  `id` int(4) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `config_types`
--

INSERT INTO `config_types` (`id`, `name`, `alias`) VALUES
(1, 'Однострочное текстовое поле', 'input'),
(2, 'Текстовое поле', 'textarea'),
(3, 'Выбор значения из списка', 'select'),
(4, 'Пароль', 'password'),
(5, 'Радиокнопка', 'radio'),
(6, 'Текстовое поле с редактором', 'tiny');

-- --------------------------------------------------------

--
-- Структура таблиці `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `tel` varchar(64) DEFAULT NULL,
  `text` text,
  `ip` varchar(16) DEFAULT NULL,
  `id_offer` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(10) NOT NULL,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `alias` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `title` text CHARACTER SET cp1251,
  `description` text CHARACTER SET cp1251,
  `keywords` text CHARACTER SET cp1251,
  `text` text CHARACTER SET cp1251,
  `status` int(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `h1` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `class` varchar(64) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `content`
--

INSERT INTO `content` (`id`, `name`, `alias`, `title`, `description`, `keywords`, `text`, `status`, `created_at`, `h1`, `updated_at`, `sort`, `parent_id`, `class`, `views`) VALUES
(1, 'Текстовая страница', 'text', 'Текстовая страница', 'Текстовая страница', 'Текстовая страница', '<h1>Заголовок h1</h1>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab eveniet, corrupti iure natus accusantium quo, aspernatur laboriosam officia repudiandae tenetur sed facere similique vero, velit? Ducimus eligendi accusantium neque saepe, iusto, quam similique atque nostrum enim sequi ex excepturi repellendus incidunt. Alias a omnis, dicta, repellat libero, eaque ea obcaecati mollitia facilis illo totam dolorum eius aut cumque tempora deserunt!</p>\r\n<h2>Заголовок h2</h2>\r\n<p>Laborum adipisci a, reiciendis, velit quia, totam doloremque perferendis accusantium quidem aut id porro repudiandae ea quos, quaerat ex? Maxime laborum, nemo nam necessitatibus ratione omnis libero sint totam deserunt perspiciatis, fugit autem non exercitationem minus. Alias omnis, iure voluptatum, tempora earum consectetur soluta vitae illum dignissimos aspernatur officia ut quod minus modi sed numquam eligendi fuga, recusandae! Odit, dignissimos.</p>\r\n<h3>Заголовок h3</h3>\r\n<p>Tempora, itaque consequuntur unde magni, ab alias totam qui est soluta, veniam, sapiente. Eligendi magni, quos praesentium optio ut quaerat sint nobis similique ab voluptas, nesciunt corporis quae repellendus maiores ratione error magnam rerum blanditiis quidem explicabo atque velit dolorum. Placeat hic, laboriosam? Tempore sequi consequuntur, fuga neque eaque qui vero debitis aliquid. Facilis iusto quod aliquid, provident inventore, ipsam!</p>\r\n<h4>Заголовок h4</h4>\r\n<p>Qui dolor est harum expedita voluptatum, possimus, ipsam tempore maxime, magnam repellendus esse. Sunt officia iste numquam nam velit, aspernatur dolorem officiis eligendi totam aliquam! Optio possimus aspernatur aliquam qui dicta minus repudiandae distinctio earum. Laboriosam eos aut accusantium iste dicta minus ratione consequatur aspernatur doloribus, temporibus doloremque earum fugit inventore dolores ipsa modi sed similique, culpa dolore cupiditate ullam.</p>\r\n<h5>Заголовок h5</h5>\r\n<p>Perspiciatis dolore totam officiis. Earum et eum optio architecto ullam laudantium placeat maxime sed, reiciendis delectus dolor veritatis, perferendis quos cupiditate pariatur odio vel accusantium itaque sunt voluptates sint ipsam, rem eveniet modi tempora. Eaque distinctio enim, ea quisquam facilis illo fuga, maxime quibusdam ut, sed dicta autem cumque molestias et eius molestiae nemo mollitia. Cumque dolorum repellendus voluptatibus aliquam!</p>\r\n<h6>Заголовок h6</h6>\r\n<p>Temporibus est dolor facere quis nisi itaque officia, consequuntur nihil, assumenda atque, magni! Eaque quae excepturi perferendis necessitatibus ipsam quibusdam repellat error, dicta pariatur quasi doloribus a libero sequi laborum illo, similique minima laudantium cum consequatur tempora blanditiis. Debitis velit dolore, voluptate similique. Omnis, assumenda nostrum laboriosam enim, provident repudiandae dolorum dicta officiis blanditiis soluta, ratione sint suscipit! Quam, quod.</p>\r\n<hr />\r\n<h2>Форматирование текста</h2>\r\n<p>Краткая демонстрация текстовых элементов / теги. Тег <code>&lt;p&gt;</code> определяет текстовый абзац(создает новый параграф). Абзац является блочным элементом, всегда начинается с новой строки и имеет отсуп до и после себя. Абзац не позволяет вкладывать внутрь себя такие элементы как address(информации об авторе), blockquote(блочная цитата), table(таблица), также другие абзаци p &gt; p.</p>\r\n<p>Если вам нужно принудительно перевести строку текста на новую линию, без создания нового абзаца и верстикальных отступов между - используйте одинарный тег <code>&lt;br&gt;</code>.</p>\r\n<p>Вы можете отформатировать текст используя такие теги как <code>&lt;i&gt; / &lt;em&gt;</code>, <em>чтобы сделать ваш текст наклонным</em>, вы также можете использовать <code>&lt;q&gt;</code> - <q>строчные цитаты, которые в отличии от блочных, позволяют вложенность внутри абзаца</q>, при помощи тегов <code>&lt;b&gt; / &lt;strong&gt;</code> - <strong>можно обозначить важный текст жирностью шрифта</strong>, для <ins>подчеркивания текста</ins> используйте <code>&lt;u&gt; / &lt;ins&gt;</code>, a для <del>перечеркивания</del> - <code>&lt;del&gt;</code>, Также можно <mark>выделить нужную часть текста</mark> при помощи тега <code>&lt;mark&gt;</code></p>\r\n<p>Чтобы превратить ваш текст в гиперссылку, нужно использовать <a title="https://webref.ru/html/a" href="https://webref.ru/html/a" target="_blank" rel="noopener noreferrer">тег &lt;a&gt;</a>. <br /> Также можно использовать специальные теги <code>&lt;sup&gt;</code> <sup>Отображает шрифт в виде верхнего индекса</sup> и <code>&lt;sub&gt;</code> <sub>Отображает шрифт в виде нижнего индекса</sub>. <br />Тег <code>&lt;small&gt;</code> <small>уменьшает размер шрифта</small></p>\r\n<p>Тег <code>&lt;code&gt;</code> предназначен для отображения одной или нескольких строк текста, который представляет собой программный код <br /> <small> <em>пример:</em> </small> <br /> - чтобы узнать контекст объекта нужно вызвать <code>console.log(this);</code>.</p>\r\n<p>Тег <kbd>&lt;kbd&gt;</kbd> используется для обозначения текста, который набирается на клавиатуре или для названия клавиш, <br /> <small> <em>пример:</em> </small> <br /> - чтобы вызвать <em>"Диспетчер задач"</em> нажмите на клавиатуре вместе 3 клавиши - <kbd>Ctrl + Alt + Del</kbd>.</p>\r\n<p>Тег <samp>&lt;samp&gt;</samp> используется для отображения текста, который является результатом вывода компьютерной программы или скрипта, <br /> <small> <em>пример:</em> </small> <br /> - в результате провеверки мы определил, что <samp>ваш браузер поддерживает JavaScript</samp></p>\r\n<p>Тег <code>&lt;abbr&gt;</code> указывает, что <abbr title="ПС - последовательность символов">ПС</abbr> является аббревиатурой. С помощью атрибута дается расшифровка сокращения - <code>title="... ваше описание"</code>, что позволяет понимать аббревиатуру тем людям, которые с ней не знакомы. Кроме того, <strong>поисковые системы индексируют полнотекстовый вариант сокращения</strong>, что может использоваться для повышения рейтинга документа.</p>\r\n<p>Как правило, когда, в документе, упоминается <dfn title="это новый термин">новый термин</dfn>, он выделяется курсивом и дается его определение. При использовании этого термина в дальнейшем, он считается уже известным читателю. Тег <dfn title="тег &lt;dfn&gt;">&lt;dfn&gt;</dfn> применяется для выделения таких терминов при их первом появлении в тексте. С помощью атрибута <kbd>title</kbd> вы можете дать более подробную расшифровку термина.</p>\r\n<address>Тег <code>&lt;address&gt;</code> предназначен для хранения информации об авторе(НЕ адреса и контакты компании, прочее) и может включать в себя любые элементы HTML вроде ссылок, текста, выделений и т.д.</address>\r\n<blockquote>Тег <code>&lt;blockquote&gt;</code> используется для обозначения цитат, длинных выписок, афоризмов и прочее, который создается в новом блоке с пробелами до и после.</blockquote>\r\n<pre>Тег &lt;pre&gt; определяет блок предварительно форматированного текста.\r\n	Такой текст отображается обычно моноширинным шрифтом и со всеми пробелами между словами.\r\n		По умолчанию, любое количество пробелов идущих в коде подряд, на веб-странице показывается как один.\r\n\r\n	Тег &lt;pre&gt; позволяет обойти эту особенность и отображать текст\r\n		как требуется разработчику.</pre>\r\n<hr />\r\n<h2>Списки</h2>\r\n<h4>Маркированный список</h4>\r\n<ul>\r\n<li><a title="Веб-студия Wezom" href="http://www.wezom.com.ua" target="_blank" rel="noopener noreferrer">Веб - студия Wezom</a> - Продвижение, создание сайтов, разработка, раскрутка сайта</li>\r\n<li><a title="Коворкинг - Центр Корова" href="http://www.korova.ks.ua" target="_blank" rel="noopener noreferrer">Коворкинг - Центр Корова</a> - Аренда офиса, рабочего места в центре Херсона</li>\r\n<li><a title="100bannerov" href="http://www.100bannerov.com" target="_blank" rel="noopener noreferrer">100bannerov</a> - Размещение бесплатной баннерной рекламы в интернете</li>\r\n<li><a title="Wezom.mobile" href="http://www.wezom.mobi" target="_blank" rel="noopener noreferrer">Wezom.mobile</a> - Разработка и создание мобильных приложений для android и ios устройств в Украине\r\n<ul>\r\n<li>Lorem ipsum dolor sit</li>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit</li>\r\n<li>Lorem ipsum dolor sit amet, consectetur\r\n<ul>\r\n<li>Повседневная практика показывает, что</li>\r\n<li>Повседневная практика показывает, что постоянный количественный рост и сфера</li>\r\n<li>Повседневная практика показывает, что постоянный количественный</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n<h4>Нумерованный список</h4>\r\n<ol>\r\n<li><a title="Областной Дом ребенка" href="http://www.dom-rebenka.org.ua" target="_blank" rel="noopener noreferrer">Областной Дом ребенка</a> - Помощь детям с пораженной ЦНС</li>\r\n<li><a title="Медицинский портал" href="http://www.blogoduma.ru" target="_blank" rel="noopener noreferrer">Медицинский портал</a> - Все о здоровье и медицин</li>\r\n<li><a title="Отдых на море" href="http://www.more.ks.ua" target="_blank" rel="noopener noreferrer">Отдых на море</a> - Черное и Азовское море / пансионаты Херсонского побережья</li>\r\n<li><a title="Рафтинг на Южном Буге и в Карпатах" href="http://www.rafting.mk.ua" target="_blank" rel="noopener noreferrer">Рафтинг на Южном Буге и в Карпатах</a> - Рафтинг в Украине, сплав по реке Южный Буг\r\n<ol>\r\n<li>Lorem ipsum dolor sit</li>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit</li>\r\n<li>Lorem ipsum dolor sit amet, consectetur\r\n<ol>\r\n<li>Повседневная практика показывает, что</li>\r\n<li>Повседневная практика показывает, что постоянный количественный рост и сфера</li>\r\n<li>Повседневная практика показывает, что постоянный количественный</li>\r\n</ol>\r\n</li>\r\n</ol>\r\n</li>\r\n</ol>\r\n<h4>Список определений</h4>\r\n<dl>\r\n<dt>Что такое Тег?</dt>\r\n<dd>Тег &mdash; это специальный символ разметки, который применяется для вставки различных элементов на веб-страницу таких как: рисунки, таблицы, ссылки и др., и для изменения их вида.</dd>\r\n<dt>Что такое HTML-документ?</dt>\r\n<dd>Обычный текстовый файл, который может содержать в себе текст, теги и стили. Изображения и другие объекты хранятся отдельно. Содержимое такого файла обычно называется HTML-код.</dd>\r\n<dt>Что такое Сайт?</dt>\r\n<dd>Cайт &mdash; это набор отдельных веб-страниц, которые связаны между собой ссылками и единым оформлением.</dd>\r\n</dl>\r\n<hr />\r\n<h2>Таблицы</h2>\r\n<table><caption>caption таблици</caption>\r\n<tbody>\r\n<tr>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n</tr>\r\n<tr>\r\n<td>Cumque ut itaque deserunt soluta.</td>\r\n<td>Tempora alias, amet error consectetur.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Tempora alias, amet error consectetur.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n<tr>\r\n<td>Atque aut maiores minima veritatis?</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Nemo possimus dolore, ipsum laborum.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Nemo possimus dolore, ipsum laborum.</td>\r\n</tr>\r\n<tr>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Harum iusto, provident libero consequatur!</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Consequuntur obcaecati numquam, perspiciatis.</td>\r\n<td>Harum iusto, provident libero consequatur!</td>\r\n</tr>\r\n<tr>\r\n<td>Quod quidem cumque repellendus ipsam!</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n<tr>\r\n<td>Quod quidem cumque repellendus ipsam!</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="table-zebra"><caption>Таблица CSS класс <strong>"table-zebra"</strong></caption>\r\n<tbody>\r\n<tr>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n</tr>\r\n<tr>\r\n<td>Cumque ut itaque deserunt soluta.</td>\r\n<td>Tempora alias, amet error consectetur.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Tempora alias, amet error consectetur.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n<tr>\r\n<td>Atque aut maiores minima veritatis?</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Nemo possimus dolore, ipsum laborum.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Nemo possimus dolore, ipsum laborum.</td>\r\n</tr>\r\n<tr>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Harum iusto, provident libero consequatur!</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Consequuntur obcaecati numquam, perspiciatis.</td>\r\n<td>Harum iusto, provident libero consequatur!</td>\r\n</tr>\r\n<tr>\r\n<td>Quod quidem cumque repellendus ipsam!</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n<tr>\r\n<td>Quod quidem cumque repellendus ipsam!</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="table-null"><caption>Таблица CSS класс <strong>"table-null"</strong></caption>\r\n<tbody>\r\n<tr>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n<th>Заглавие таблици</th>\r\n</tr>\r\n<tr>\r\n<td>Cumque ut itaque deserunt soluta.</td>\r\n<td>Tempora alias, amet error consectetur.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Tempora alias, amet error consectetur.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n<tr>\r\n<td>Atque aut maiores minima veritatis?</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Nemo possimus dolore, ipsum laborum.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Nemo possimus dolore, ipsum laborum.</td>\r\n</tr>\r\n<tr>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Harum iusto, provident libero consequatur!</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Consequuntur obcaecati numquam, perspiciatis.</td>\r\n<td>Harum iusto, provident libero consequatur!</td>\r\n</tr>\r\n<tr>\r\n<td>Quod quidem cumque repellendus ipsam!</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n<tr>\r\n<td>Quod quidem cumque repellendus ipsam!</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n<td>Neque consequuntur doloribus nostrum autem.</td>\r\n<td>Lorem ipsum dolor sit amet.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2>Media</h2>\r\n<h4>Iframe</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est in, obcaecati nulla amet distinctio, sapiente itaque porro neque at laudantium temporibus quam magnam. Animi rem maxime autem quaerat, facilis porro doloremque aspernatur asperiores error praesentium. Aliquam odit harum, aperiam deleniti.</p>\r\n<p><iframe src="https://www.youtube.com/embed/LEzKfmzKmjA" width="1280" height="720" allowfullscreen="allowfullscreen"></iframe></p>\r\n<h4>HTML 5 Video</h4>\r\n<p>Rem aspernatur, blanditiis, magni quibusdam laudantium facere voluptates cum saepe atque, fugiat, vero. In magni repudiandae laboriosam quam. Eos fugit minima molestias, ipsam natus asperiores in, dolor eveniet ex, officia assumenda ratione repellendus fuga. Dolor neque, fugit nam minima illo.</p>\r\n<p><video src="Media/media-files/video.mp4" controls="controls" width="1280" height="720"></video></p>\r\n<h4>HTML 5 Audio</h4>\r\n<p>Voluptate laboriosam aperiam, odit voluptatem. Nobis eius error eaque velit voluptatum quis facere perferendis aut corporis doloribus at voluptate, qui itaque quas, omnis iste mollitia. Suscipit atque unde, incidunt illum dolores illo dolor distinctio ad voluptates qui dicta sequi natus?</p>\r\n<p><audio src="Media/media-files/audio.mp3" controls="controls"></audio></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus suscipit sint hic alias doloribus sit deleniti sunt, est soluta autem reprehenderit facere itaque. Doloribus quam est debitis earum qui, voluptas a asperiores porro reiciendis adipisci, repellat unde ipsam laboriosam, nisi sequi dolorum facere, quod provident! Eligendi facilis eum ipsa officia sapiente incidunt nostrum aperiam, consequatur cupiditate esse exercitationem, eos repellat.</p>\r\n<hr />\r\n<h2>Content images</h2>\r\n<p><img style="float: left; margin: 0 20px 20px 0;" title="Ширина изображения 120px" src="Media/images/no-image.png" alt="" width="120" /></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate distinctio quasi hic, numquam ratione! Corporis modi accusantium aliquid ex. Sunt alias commodi dolore unde fugiat, aperiam, itaque neque ex autem ullam dolor. Accusantium aperiam voluptatibus quis obcaecati expedita assumenda corporis adipisci illum iure, ipsa doloribus dolorem ipsum cum enim repellat officia perferendis veniam optio fugiat, molestias similique maxime autem dolorum. Incidunt debitis earum commodi optio, possimus perferendis, odio perspiciatis voluptatum atque nam iusto, quae provident. Expedita dolorem facilis, perferendis, provident quos tenetur et animi fugiat cumque cupiditate explicabo magnam sapiente corporis. Quia nulla fugiat ipsam maiores iusto cumque ad tempore tenetur quis debitis voluptate aliquid voluptatibus optio explicabo quos, officia facilis dolore porro.</p>\r\n<div class="fclear">&nbsp;</div>\r\n<p><img style="float: left; margin: 0 20px 20px 0;" title="Ширина изображения 220px" src="Media/images/no-image.png" alt="" width="220" /></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate distinctio quasi hic, numquam ratione! Corporis modi accusantium aliquid ex. Sunt alias commodi dolore unde fugiat, aperiam, itaque neque ex autem ullam dolor. Accusantium aperiam voluptatibus quis obcaecati expedita assumenda corporis adipisci illum iure, ipsa doloribus dolorem ipsum cum enim repellat officia perferendis veniam optio fugiat, molestias similique maxime autem dolorum. Incidunt debitis earum commodi optio, possimus perferendis, odio perspiciatis voluptatum atque nam iusto, quae provident. Expedita dolorem facilis, perferendis, provident quos tenetur et animi fugiat cumque cupiditate explicabo magnam sapiente corporis. Quia nulla fugiat ipsam maiores iusto cumque ad tempore tenetur quis debitis voluptate aliquid voluptatibus optio explicabo quos, officia facilis dolore porro.</p>\r\n<div class="fclear">&nbsp;</div>\r\n<p><img style="float: left; margin: 0 20px 20px 0;" title="Ширина изображения 320px" src="Media/images/no-image.png" alt="" width="320" /></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate distinctio quasi hic, numquam ratione! Corporis modi accusantium aliquid ex. Sunt alias commodi dolore unde fugiat, aperiam, itaque neque ex autem ullam dolor. Accusantium aperiam voluptatibus quis obcaecati expedita assumenda corporis adipisci illum iure, ipsa doloribus dolorem ipsum cum enim repellat officia perferendis veniam optio fugiat, molestias similique maxime autem dolorum. Incidunt debitis earum commodi optio, possimus perferendis, odio perspiciatis voluptatum atque nam iusto, quae provident. Expedita dolorem facilis, perferendis, provident quos tenetur et animi fugiat cumque cupiditate explicabo magnam sapiente corporis. Quia nulla fugiat ipsam maiores iusto cumque ad tempore tenetur quis debitis voluptate aliquid voluptatibus optio explicabo quos, officia facilis dolore porro.</p>\r\n<div class="fclear">&nbsp;</div>\r\n<p><img style="float: left; margin: 0 20px 20px 0;" title="Ширина изображения 420px" src="Media/images/no-image.png" alt="" width="420" /></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate distinctio quasi hic, numquam ratione! Corporis modi accusantium aliquid ex. Sunt alias commodi dolore unde fugiat, aperiam, itaque neque ex autem ullam dolor. Accusantium aperiam voluptatibus quis obcaecati expedita assumenda corporis adipisci illum iure, ipsa doloribus dolorem ipsum cum enim repellat officia perferendis veniam optio fugiat, molestias similique maxime autem dolorum. Incidunt debitis earum commodi optio, possimus perferendis, odio perspiciatis voluptatum atque nam iusto, quae provident. Expedita dolorem facilis, perferendis, provident quos tenetur et animi fugiat cumque cupiditate explicabo magnam sapiente corporis. Quia nulla fugiat ipsam maiores iusto cumque ad tempore tenetur quis debitis voluptate aliquid voluptatibus optio explicabo quos, officia facilis dolore porro.</p>\r\n<div class="fclear">&nbsp;</div>\r\n<p><img style="float: left; margin: 0 20px 20px 0;" title="Ширина изображения 520px" src="Media/images/no-image.png" alt="" width="520" /></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate distinctio quasi hic, numquam ratione! Corporis modi accusantium aliquid ex. Sunt alias commodi dolore unde fugiat, aperiam, itaque neque ex autem ullam dolor. Accusantium aperiam voluptatibus quis obcaecati expedita assumenda corporis adipisci illum iure, ipsa doloribus dolorem ipsum cum enim repellat officia perferendis veniam optio fugiat, molestias similique maxime autem dolorum. Incidunt debitis earum commodi optio, possimus perferendis, odio perspiciatis voluptatum atque nam iusto, quae provident. Expedita dolorem facilis, perferendis, provident quos tenetur et animi fugiat cumque cupiditate explicabo magnam sapiente corporis. Quia nulla fugiat ipsam maiores iusto cumque ad tempore tenetur quis debitis voluptate aliquid voluptatibus optio explicabo quos, officia facilis dolore porro.</p>\r\n<div class="fclear">&nbsp;</div>\r\n<hr />', 1, 1510405278, 'Текстовая страница', 1510651165, 0, 0, NULL, 48),
(2, 'Политика конфидециальности', 'policy', '', '', '', '<p>Политика конфидециальности</p>', 1, 1510407400, '', 1510411142, 0, 0, NULL, 26);

-- --------------------------------------------------------

--
-- Структура таблиці `control`
--

DROP TABLE IF EXISTS `control`;
CREATE TABLE IF NOT EXISTS `control` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `alias` varchar(32) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `other` text,
  `updated_at` int(11) DEFAULT NULL,
  `breadcrumb` varchar(255) DEFAULT NULL,
  `sitemapxml` int(1) DEFAULT NULL,
  `name_page` text,
  `title_cust` varchar(255) DEFAULT NULL,
  `first_advantage` text,
  `second_advantage` text,
  `third_advantage` text,
  `text_for_widget` text,
  `title_top` text,
  `text_top` text,
  `image` text,
  `pdf` varchar(255) DEFAULT NULL,
  `xls` varchar(255) DEFAULT NULL,
  `size_of_pdf` text,
  `size_of_xls` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `control`
--

INSERT INTO `control` (`id`, `name`, `h1`, `title`, `keywords`, `description`, `text`, `alias`, `status`, `other`, `updated_at`, `breadcrumb`, `sitemapxml`, `name_page`, `title_cust`, `first_advantage`, `second_advantage`, `third_advantage`, `text_for_widget`, `title_top`, `text_top`, `image`, `pdf`, `xls`, `size_of_pdf`, `size_of_xls`) VALUES
(1, 'Главная страница', 'Главная страница', 'Главная страница', 'Главная страница', 'Главная страница', '<p>Плиты и блоки<span class="is-select">пенополистирольные</span> от производителя</p>', 'index', 1, NULL, 1510503303, 'Главная', 1, 'Предлагаем листы пенополистирола толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', NULL, '', '', '', NULL, NULL, NULL, '7281b2cc6778510c73977687f0d004e9.png', NULL, NULL, NULL, NULL),
(2, 'Контакты', 'Контакты', 'Контакты', 'Контакты', 'Контакты', '<div class="cell cell--12 cell--sm24">\r\n<div class="grid i-20 i-sm10">\r\n<div class="cell cell--24">\r\n<div class="section14__title">Контактная информация</div>\r\n<div class="section14__line mt-20">&nbsp;</div>\r\n</div>\r\n<div class="cell cell--12 cell--ms24">\r\n<div class="grid i-10">\r\n<div class="cell cell--24">\r\n<div class="section14__head">Юридический адрес:</div>\r\n<div class="section14__text">\r\n<div>\r\n<p>07100, Киевская область, г. Славутич, Киевский квартал, д.6, кв.14</p>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="cell cell--24">\r\n<div class="section14__head">Контактные телефоны:</div>\r\n<div class="section14__text">\r\n<div><a class="section14__tel" href="tel:+380447926999">+38 (04479) 2-69-99</a>, <a class="section14__tel" href="tel:+380447929700">2-97-00</a></div>\r\n<div>E-mail:<a class="section14__email" href="mailto:polimer@slavutich.kiev.ua">polimer@slavutich.kiev.ua</a></div>\r\n<div><a class="section14__email" href="http://www.penoplast.maket.net/">http://www.penoplast.maket.net/</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="cell cell--12 cell--ms24">\r\n<div class="grid i-10">\r\n<div class="cell cell--24">\r\n<div class="section14__head">Директор - Якусевич Сергей</div>\r\n<div class="section14__text">\r\n<div>Мобильный:<a class="section14__tel" href="tel:+380503109717">(050) 310-97-17</a></div>\r\n<div>E-mail:<a class="section14__email" href="mailto:00069km@ukr.net">00069km@ukr.net</a></div>\r\n</div>\r\n</div>\r\n<div class="cell cell--24">\r\n<div class="section14__head">Заместитель директора по продажам - Катамай Анатолий</div>\r\n<div class="section14__text">\r\n<div>Мобильный:<a class="section14__tel" href="tel:+380503589802">(050) 358-98-02</a></div>\r\n<div>Рабочий:<a class="section14__tel" href="tel:+380447926999">(04479) 2-69-99</a></div>\r\n<div>E-mail:<a class="section14__email" href="mailto:polimer@slavutich.kiev.ua">polimer@slavutich.kiev.ua</a></div>\r\n</div>\r\n</div>\r\n<div class="cell cell--24">\r\n<div class="section14__head">Менеджер по продажам - Радченко Елена</div>\r\n<div class="section14__text">\r\n<div>Мобильный:<a class="section14__tel" href="tel:+380505528802">(050) 552-88-02</a></div>\r\n<div>Рабочий:<a class="section14__tel" href="tel:+380447926999">(04479) 2-69-99</a>, <a class="section14__tel" href="tel:+380447929700">2-97-00</a></div>\r\n<div>E-mail:<a class="section14__email" href="mailto:00069km@ukr.net">polimer@slavutich.kiev.ua</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'contacts', 1, '{"longitude":"30.756944299999986","latitude":"51.52055559999999","zoom":"14"}', 1510398596, NULL, 1, 'ПОЛИМЕР-СЛАВУТИЧ, ООО Адрес: Киевская обл., г. Славутич, квар. Черниговский, 351', 'Контакты', NULL, NULL, NULL, 'Мы готовы рассмотреть и выполнить в кратчайшие сроки любые Ваши заявки. Оставьте заявку, мы перезвоним и всё с вами обсудим!', 'Контакты', 'Перед заказом вы можете приехать, проверить качество продукции и наличие сертификатов', '133ba923f4ad6e339a17970006ee67b5.jpg', NULL, NULL, NULL, NULL),
(3, 'Сертификаты', 'Сертификаты', 'Сертификаты', 'Сертификаты', 'Сертификаты', NULL, 'sertificates', 1, NULL, 1510154732, NULL, 1, ' Деятельность Компании осуществляется на основании следующих разрешительных документов', 'ВСЯ ПРОДУКЦИЯ СЕРТИФИЦИРОВАНА', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Секция "Наши преимущества"', 'Секция "Наши преимущества"', 'Секция "Наши преимущества"', 'Секция "Наши преимущества"', 'Секция "Наши преимущества"', NULL, 'our_best', 1, NULL, 1510154364, NULL, 0, '', 'Наши преимущества', '<div class="section4__name">\r\n<span>Низкая цена</span>\r\n</div>\r\n<div class="section4__desc">\r\n<p>При сотрудничестве с производителем вы определенно будете иметь преимущества в цене. Потому что отсутствует наценка посредника.</p>\r\n</div>', '<div class="section4__name">\r\n<span>Высокое качество и профессионализм</span>\r\n</div>\r\n<div class="section4__desc">\r\n<p>Лучшие в своей сфере специалисты, оборудование и инструменты. Вся наши продукция полностью сертифицирована.</p>\r\n </div>', '<div class="section4__name">\r\n<span>Быстрая доставка</span>\r\n</div>\r\n<div class="section4__desc">\r\n<p>В зависимости от объемов заказа сроки на его изготовление занимают 1-5 дней. Скорость доставки для вас позволяет обеспечивать собственный автотранспорт.</p>\r\n</div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Доставка', 'Доставка', 'Доставка', 'Доставка', 'Доставка', '<p>Оборудование по раскрою блоков позволяет выпускать листы толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.</p>\r\n<p>Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.</p>\r\n<p><strong>Мы обеспечиваем надежную доставку вашего заказа:</strong> <img style="float: right; margin: 30px;" title="image" src="http://wezom.cms/Media/files/filemanager/1111.png" alt="image" width="400" height="329" /></p>\r\n<ul>\r\n<li>листы упаковываются в пачки и заворачиваются в плёнку;</li>\r\n<li>укладываются на деревянные поддоны, либо стягиваются брусками;</li>\r\n<li>дополнительно защищаем упаковочной пленкой;</li>\r\n<li>чтобы избежать соскальзывания паллет с кузова, наши такелажники укрепляют их с помощью стяжных ремней и накрывают полиэтиленом или брезентом;</li>\r\n<li>водители соблюдают скоростной режим и объезжают неровности.</li>\r\n</ul>', 'delivery', 1, NULL, 1510327180, NULL, 1, 'Осуществляем доставку по всей территории Украины в кратчайшие сроки.', 'КАЧЕСТВО ВАШЕГО ЗАКАЗА В БЕЗОПАСНОСТИ!', NULL, NULL, NULL, NULL, '<span>Доставка</span>\r\n<div>пенополистирольных плит и блоков</div>', 'Доставка осуществляется на следующий день, 7 дней в неделю. Скорость доставки обеспечивает собственный автотранспорт.', '2a5406cd8f2054e298dde1705607535e.jpg', NULL, NULL, NULL, NULL),
(7, 'Карта сайта', 'Карта сайта', 'Карта сайта', 'Карта сайта', 'Карта сайта', NULL, 'sitemap', 1, NULL, NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Продукция', 'Продукция', 'Продукция', 'Продукция', 'Продукция', NULL, 'products', 1, NULL, 1510308288, NULL, 1, 'Сертификаты можно посмотреть ниже. Плиты пенополистирольные типа ПСБ-С-15, ПСБ-С-25, ПСБ-С-35 и ПСБ-С-50', 'Ассортимент нашей продукции', NULL, NULL, NULL, NULL, 'Наша продукция', 'Наша компания производит плиты пенополистирольные типа ПСБ-С-15, ПСБ-С-25, ПСБ-С-35 и ПСБ-С-50 согласно ДСТУ Б В.2.7-8-94', '53d6af9c7d46c90225c6d876aaea9a6f.jpg', NULL, NULL, NULL, NULL),
(11, 'Прайс-лист', 'Прайс-лист', 'Прайс-лист', 'Прайс-лист', 'Прайс-лист', '<p>Минимальная расчетная стоимость заказа на услуги &ndash; 30000 рублей.</p>\r\n<p>Все цены в таблице являются ориентировочными. В зависимости от сложности и величины заказа они могут корректироваться.</p>', 'price', 1, NULL, 1510564197, NULL, 1, '', NULL, NULL, NULL, NULL, NULL, '<span>Прайс-лист</span>\r\n<div>на плиты и блоки пенополистирольные</div>', 'Предлагаем листы пенополистирола толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', '999a8ff54204ed4ee42aca6337acdd4e.jpg', 'eda81b8ecbd9525020b3a8386cf4da70.pdf', '0bf72ad5a215aa7483412b107127526e.xls', '330.30 KB', '22.50 KB'),
(12, 'О компании', 'О компании', 'О компании', 'О компании', 'О компании', '<p>Оборудование по раскрою блоков позволяет выпускать листы толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.</p>\r\n<p>Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.</p>\r\n<p>Готовы рассмотреть и выполнить в кратчайшие сроки любые Ваши заявки. Для этого Вам необходимо всего лишь заполнить бланк заказа или связаться с нами по контактным телефонам.</p>', 'about', 1, NULL, 1510308319, NULL, 1, 'Наша компания производит плиты пенополистирольные типа ПСБ-С-15, ПСБ-С-25, ПСБ-С-35 и ПСБ-С-50 согласно ДСТУ Б В.2.7-8-94', 'Полимер Славутич – Ваш надежный партнер', NULL, NULL, NULL, NULL, '<span>Плиты и блоки</span>\r\n<span class="is-select">пенополистирольные</span>\r\n<span>от производителя</span>', 'Предлагаем листы пенополистирола толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', '93d28705bb3b0eb0aa6aa15a170f579f.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `cron`
--

DROP TABLE IF EXISTS `cron`;
CREATE TABLE IF NOT EXISTS `cron` (
  `id` int(10) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `alias` varchar(255) NOT NULL,
  `text` text,
  `sort` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `gallery`
--

INSERT INTO `gallery` (`id`, `created_at`, `updated_at`, `status`, `name`, `image`, `h1`, `title`, `keywords`, `description`, `alias`, `text`, `sort`) VALUES
(5, 1447092527, 1510586165, 1, 'Галерея для страницы "О компании"', 'f35ba04c4c18ed371a206975ff16ce90.jpg', 'Мужская обувь', 'Мужская обувь', 'Мужская обувь', 'Мужская обувь', 'galereja-dlja-stranitsy-o-kompanii', '', NULL),
(6, 1447092625, 1510586198, 1, 'О компании (слайдер на главной)', 'e85a9a463467065e7693edbca081158b.jpg', 'О компании', 'О компании', 'О компании', 'Женская обувь', 'o-kompanii', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `image` varchar(128) NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_id` int(10) NOT NULL,
  `sort` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `created_at`, `updated_at`, `image`, `main`, `gallery_id`, `sort`) VALUES
(41, 1510139919, NULL, 'dd3caf6f94855f466b99d085c218e786.jpg', 0, 6, 1),
(42, 1510139919, NULL, '8d808df329ae8a9f530337c19a10996c.jpg', 0, 6, 2),
(43, 1510139920, NULL, '2090fb99ef0ab821bb0bd2f65cfd5212.jpg', 0, 6, 3),
(62, 1510301793, NULL, 'adf2dee0250851368bf17b70215c4e64.jpg', 0, 5, 1),
(63, 1510301793, NULL, 'd00d9a80f0336abf06790966c8b0fc1c.jpg', 0, 5, 2),
(64, 1510301794, NULL, '6ae1765a6447eaf7e3a856f8d83c0031.jpg', 0, 5, 3),
(65, 1510301794, NULL, '3bef6f50b71e769dd47c4fbbb627c9ba.jpg', 0, 5, 4),
(66, 1510301794, NULL, '3c8ec1ba3291f96f3709c7006154a4c4.jpg', 0, 5, 5),
(67, 1510301795, NULL, '83072317741024743947ac725de55c64.jpg', 0, 5, 6);

-- --------------------------------------------------------

--
-- Структура таблиці `i18n`
--

DROP TABLE IF EXISTS `i18n`;
CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(2) NOT NULL,
  `alias` varchar(3) NOT NULL,
  `name` varchar(32) NOT NULL,
  `short_name` varchar(16) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `i18n`
--

INSERT INTO `i18n` (`id`, `alias`, `name`, `short_name`, `locale`, `default`) VALUES
(1, 'ru', 'Русский', 'РУС', 'ru-ru', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `instructions`
--

DROP TABLE IF EXISTS `instructions`;
CREATE TABLE IF NOT EXISTS `instructions` (
  `id` int(5) NOT NULL,
  `module` varchar(75) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `instructions`
--

INSERT INTO `instructions` (`id`, `module`, `link`) VALUES
(1, 'config', 'https://www.youtube.com/embed/ei0jwyvSwnI'),
(2, 'MailTemplates', 'https://www.youtube.com/embed/Ysiua0gHyK4'),
(3, 'log', 'https://www.youtube.com/embed/_BCCa7I8RS4'),
(4, 'callback', 'https://www.youtube.com/embed/KYVLmS8npXw'),
(5, 'blacklist', 'https://www.youtube.com/embed/8mRSL9kCfVU'),
(6, 'reviews', 'https://www.youtube.com/embed/dD_Xu8QokB8'),
(7, 'subscribe', 'https://www.youtube.com/embed/u97oKxJi-fs'),
(8, 'subscribers', 'https://www.youtube.com/embed/u97oKxJi-fs');

-- --------------------------------------------------------

--
-- Структура таблиці `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `log`
--

INSERT INTO `log` (`id`, `created_at`, `updated_at`, `name`, `link`, `ip`, `deleted`, `type`, `status`) VALUES
(1, 1510561722, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/1', '127.0.0.1', 0, 2, 0),
(2, 1510588782, NULL, 'Заказ звонка', '/wezom/callback/edit/1', '127.0.0.1', 0, 3, 0),
(3, 1510588903, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/1', '127.0.0.1', 0, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `mail_templates`
--

DROP TABLE IF EXISTS `mail_templates`;
CREATE TABLE IF NOT EXISTS `mail_templates` (
  `id` int(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `mail_templates`
--

INSERT INTO `mail_templates` (`id`, `name`, `subject`, `text`, `updated_at`, `status`, `sort`) VALUES
(1, 'Контактная форма ( Администратору )', 'Новое сообщение из контактной формы - сайт {{site}}', '<p>Вам пришло новое письмо из контактной формы с сайта {{site}}!</p>\r\n<p>Имя отправителя: {{name}} ( {{ip}} ).</p>\r\n<p>Телефон отправителя: {{tel}}.</p>\r\n<p>IP отправителя: {{ip}}.</p>\r\n<p>Дата сообщения: {{date}}.</p>\r\n<p>Текст сообщения: {{text}}.</p>\r\n<p>&nbsp;</p>\r\n<p>Письмо сгенерировано автоматически. Пожалуйста не отвечайте на него!</p>', 1510244093, 1, 0),
(3, 'Заказ звонка ( Администратору )', 'Заказ звонка, сайт {{site}}', '<p>Поступил новый заказ звонка с сайта {{site}}!</p>\r\n<p>Данные заказчика:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>Имя:</td>\r\n<td>{{name}}</td>\r\n</tr>\r\n<tr>\r\n<td>Номер телефона:</td>\r\n<td>{{phone}}</td>\r\n</tr>\r\n<tr>\r\n<td>IP:</td>\r\n<td>{{ip}}</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1461528132, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(10) NOT NULL,
  `id_parent` int(11) DEFAULT '0',
  `name` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `id_content` int(11) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `image` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `count` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `menu`
--

INSERT INTO `menu` (`id`, `id_parent`, `name`, `link`, `sort`, `id_content`, `created_at`, `status`, `image`, `updated_at`, `icon`, `count`) VALUES
(1, 0, 'Панель управления', 'index/index', 0, 0, NULL, 1, NULL, NULL, 'fa-dashboard', NULL),
(2, 0, 'SEO', NULL, 16, 0, NULL, 1, '', NULL, 'fa-tags', NULL),
(3, 0, 'Пользователи', 'users/index', 10, 0, NULL, 1, NULL, NULL, 'fa-users', NULL),
(4, 0, 'Настройки сайта', 'config/edit', 15, 0, NULL, 1, NULL, NULL, 'fa-cogs', NULL),
(5, 0, 'Контент', NULL, 1, 0, NULL, 1, NULL, NULL, 'fa-folder-open-o', NULL),
(6, 5, 'Список текстовых страниц', 'content/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(8, 2, 'Метрика и счетчики', 'seo_scripts/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(12, 2, 'Теги для конкретных ссылок', 'seo_links/index', 0, 0, NULL, 1, NULL, NULL, NULL, NULL),
(14, 0, 'Шаблоны писем', 'mailTemplates/index', 14, 0, NULL, 1, NULL, NULL, 'fa-file-image-o', NULL),
(15, 0, 'Меню', 'menu/index', 4, 0, NULL, 1, NULL, NULL, 'fa-list-ul', NULL),
(28, 64, 'Список банеров', 'banners/index', 5, 0, NULL, 1, NULL, NULL, NULL, NULL),
(30, 0, 'Рассылка', NULL, 11, 0, NULL, 0, NULL, NULL, 'fa-rss', NULL),
(31, 30, 'Список подписчиков', 'subscribers/index', 1, 0, NULL, 0, NULL, NULL, NULL, NULL),
(32, 30, 'Добавить подписчика', 'subscribers/add', 2, 0, NULL, 0, NULL, NULL, NULL, NULL),
(33, 30, 'Список разосланных писем', 'subscribe/index', 3, 0, NULL, 0, NULL, NULL, NULL, NULL),
(34, 30, 'Разослать письмо', 'subscribe/send', 4, 0, NULL, 0, NULL, NULL, NULL, NULL),
(35, 0, 'Обратная связь', NULL, 12, 0, NULL, 1, NULL, NULL, 'fa-envelope-o', 'all_emails'),
(36, 35, 'Сообщения из контактной формы', 'contacts/index', 1, 0, NULL, 1, NULL, NULL, NULL, 'contacts'),
(37, 35, 'Заказы звонка', 'callback/index', 2, 0, NULL, 1, NULL, NULL, NULL, 'callbacks'),
(38, 0, 'Каталог', NULL, 7, 0, NULL, 1, NULL, NULL, 'fa-inbox', NULL),
(39, 38, 'Группы товаров', 'groups/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(41, 38, 'Товары', 'items/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(54, 0, 'Лента событий', 'log/index', 13, 0, NULL, 1, NULL, NULL, 'fa-tasks', NULL),
(57, 2, 'Шаблоны', 'seo_templates/index', -2, 0, NULL, 0, NULL, NULL, NULL, NULL),
(59, 5, 'Системные страницы', 'control/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(62, 64, 'Список альбомов', 'gallery/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(64, 0, 'Мультимедиа', NULL, 6, 0, NULL, 1, NULL, NULL, 'fa-picture-o', NULL),
(65, 3, 'Список пользователей', 'users/index', 1, 0, NULL, 0, NULL, NULL, NULL, NULL),
(66, 3, 'Список администраторов', 'admins/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(68, 3, 'Список ролей', 'roles/index', 5, 0, NULL, 1, NULL, NULL, NULL, NULL),
(71, 2, 'Перенаправления', 'seo_redirects/index', 6, 0, NULL, 1, NULL, NULL, NULL, NULL),
(108, 0, 'Статистика', '', 999, 0, NULL, 1, '', NULL, 'fa-signal', NULL),
(109, 108, 'Посетители сайта', 'visitors/index', 1, 0, NULL, 1, '', NULL, '', NULL),
(110, 108, 'Переходы по сайту', 'hits/index', 2, 0, NULL, 1, '', NULL, '', NULL),
(111, 108, 'Рефереры', 'referers/index', 3, 0, NULL, 1, '', NULL, '', NULL),
(112, 108, 'Статистика по пользователям', 'statistic/users', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(113, 108, 'Статистика по товарам', 'statistic/items', 5, 0, NULL, 1, NULL, NULL, NULL, NULL),
(121, 0, 'Черный список', NULL, 10, 0, NULL, 1, NULL, NULL, 'fa-file', NULL),
(122, 121, 'Заблокированные адреса', 'blacklist/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(123, 121, 'Заблокировать адрес', 'blacklist/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(127, 0, 'Переводы', 'btranslates', 15, 0, NULL, 0, NULL, NULL, 'fa-language', NULL),
(128, 2, 'Файлы', 'seo_files/index', 8, 0, NULL, 1, NULL, NULL, NULL, NULL),
(130, 2, 'Карта сайта', 'sitemap/index', 10, 0, NULL, 1, NULL, NULL, NULL, NULL),
(131, 127, 'Переводы сайта', 'translates', NULL, 0, NULL, 0, NULL, NULL, NULL, NULL),
(132, 127, 'Переводы админки', 'btranslates', NULL, 0, NULL, 0, NULL, NULL, NULL, NULL),
(133, 0, 'Справочник', NULL, 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(134, 133, 'Сертификаты', 'sertificates/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(135, 133, 'Партнеры', 'partners/index', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(136, 133, 'Услуги', 'offers/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `popup_messages`
--

DROP TABLE IF EXISTS `popup_messages`;
CREATE TABLE IF NOT EXISTS `popup_messages` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `ru` text,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `popup_messages`
--

INSERT INTO `popup_messages` (`id`, `name`, `ru`, `updated_at`) VALUES
(1, 'message_after_oformleniya_basket', '<p>Благодарим вас за заказ! <br /> Менеджер свяжется с вами в ближайшее время.</p>', 2013),
(2, 'message_error_captcha', 'Вы неправильно ввели код безопасности.<br> Повторите, пожалуйста, отправку данных, внимательно указав код.', NULL),
(3, 'message_after_q_about_good', 'Ваш вопрос принят. Менеджер ответит вам в ближайшее время.', NULL),
(4, 'message_add_contact', 'Благодарим вас за сообщение!', NULL),
(5, 'message_add_comment_to_guestbook', 'Благодарим вас за оставленный отзыв. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.', NULL),
(6, 'message_add_comment_to_news', 'Благодарим вас за оставленный комментарий. <br>С вашей помощью наш сайт становится интереснее и полезнее. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.', NULL),
(7, 'message_error_login', 'Неправильно введен логин', NULL),
(8, 'message_after_registration', 'Благодарим вас за регистрацию на нашем сайте! Приятной работы!', NULL),
(9, 'message_text_after_registration_user_at_site', 'Благодарим вас за регистрацию на нашем сайте! <br /> На ваш email, указанный при регистрации, отправлено уведомление с данными для входа в ваш личный кабинет на сайте. <br /> Приятной работы!', NULL),
(10, 'message_after_autorisation', 'Данные введены правильно! Приятной работы!', NULL),
(11, 'message_text_after_autorisation_user_at_site', 'Добро пожаловать на наш сайт! <br /> Воспользуйтесь личным кабинетом для: редактирования своих данных и просмотра истории покупок. <br /> Приятной работы!', NULL),
(12, 'message_error_autorisation', 'Данные введены неправильно! Будьте внимательны!', NULL),
(13, 'message_after_exit', 'Возвращайтесь еще!', NULL),
(14, 'message_text_after_exit', 'Администрация сайта благодарит вас за время, потраченное на нашем сайте. До скорых встреч!', NULL),
(16, 'message_after_edit_data', 'Выши данные изменены. Приятной работы.', NULL),
(17, 'message_text_after_edit_data', 'Благодарим вас внимание к нашему сайту. <br /> На ваш email, указанный при регистрации, отправлено уведомление с измененными данными. <br /> Приятной работы!', NULL),
(18, 'subscribe_refuse', 'Вы отказались от рассылки на сайте!', NULL),
(19, 'subscribe_refuse_error', 'Вы не подписывались на рассылку на сайте!', NULL),
(20, 'subscribe_already01', 'уже является подписчиком на сайте!', NULL),
(21, 'subscribe_already02', 'введите другую почту для подписки.', NULL),
(22, 'subscribe_done', 'Вы подписались на рассылку на сайте', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `seo_links`
--

DROP TABLE IF EXISTS `seo_links`;
CREATE TABLE IF NOT EXISTS `seo_links` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `seo_redirects`
--

DROP TABLE IF EXISTS `seo_redirects`;
CREATE TABLE IF NOT EXISTS `seo_redirects` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `link_from` varchar(255) DEFAULT NULL,
  `link_to` varchar(255) DEFAULT NULL,
  `type` int(4) NOT NULL DEFAULT '300'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `seo_scripts`
--

DROP TABLE IF EXISTS `seo_scripts`;
CREATE TABLE IF NOT EXISTS `seo_scripts` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `script` text,
  `status` int(1) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `place` varchar(31) DEFAULT 'head'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `seo_templates`
--

DROP TABLE IF EXISTS `seo_templates`;
CREATE TABLE IF NOT EXISTS `seo_templates` (
  `id` int(10) NOT NULL,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `h1` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `seo_templates`
--

INSERT INTO `seo_templates` (`id`, `name`, `h1`, `title`, `description`, `keywords`, `updated_at`) VALUES
(1, 'Шаблон для групп товаров', '{{name}}', '{{name}} в интернет магазине Airpac. Купить обувь в Украине, Киеве', '{{content:20}}', '{{name}}, Купить обувь в Украине, Киеве', 1431017014),
(2, 'Шаблон для товаров', '{{name}}', '{{name}} в интернет магазине Airpac. Купить лучшую обувь {{group}} в Украине, Киеве от {{price}} грн', 'Купить {{name}} в Украине. Огромный ассортимент обуви, современные {{group}} от компании {{brand}}. Гарантия качества, своевременная доставка, любые способы оплаты от {{price}} грн', 'Купить, {{name}}, {{group}}, {{brand}}, обувь от {{price}} грн', 1492428073),
(3, 'Шаблон для производителей', '{{name}}', 'Шаблон для title производителя {{name}}', 'Шаблон для description производителя {{name}}', 'Шаблон для keywords производителя {{name}}', 1492428398),
(4, 'Шаблон для новостей', '{{name}}', 'Шаблон для title новости {{name}}', 'Шаблон для description новости  {{name}}', 'Шаблон для keywords новости {{name}}', 1509459656),
(5, 'Шаблон для статей', '{{name}}', 'Шаблон для title статьи {{name}}', 'Шаблон для description статьи {{name}}', 'Шаблон для keywords статьи {{name}}', 1509459640),
(6, 'Шаблон для фильтра товаров', '{{group}} - {{filter}}', '{{group}} - {{filter}} {{site}}', '{{group}} - {{filter}} {{site}}', '{{group}} - {{filter}} {{site}}', 1509523491);

-- --------------------------------------------------------

--
-- Структура таблиці `sitemap`
--

DROP TABLE IF EXISTS `sitemap`;
CREATE TABLE IF NOT EXISTS `sitemap` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `tpl` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sitemap`
--

INSERT INTO `sitemap` (`id`, `alias`, `name`, `parent_id`, `status`, `updated_at`, `sort`, `tpl`) VALUES
(1, '/', 'Главная страница', 0, 1, NULL, 2, ''),
(24, '404', 'Страница ошибки 404', 1, 1, NULL, 1, ''),
(25, 'about', 'О компании', 1, 1, NULL, 2, 'About'),
(26, 'products', 'Продукция', 1, 1, NULL, 3, 'Products'),
(27, 'price', 'Прайс-лист', 1, 1, NULL, 4, 'Price'),
(28, 'delivery', 'Доставка', 1, 1, NULL, 5, 'Delivery'),
(29, 'contacts', 'Контакты', 1, 1, NULL, 6, 'Contacts'),
(30, 'text', 'Текст', 1, 1, NULL, 7, 'Text');

-- --------------------------------------------------------

--
-- Структура таблиці `sitemenu`
--

DROP TABLE IF EXISTS `sitemenu`;
CREATE TABLE IF NOT EXISTS `sitemenu` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sitemenu`
--

INSERT INTO `sitemenu` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `url`) VALUES
(6, 1447091549, 1510055087, 1, 2, 'Прайс-лист', '/price'),
(7, 1447091558, 1510055041, 1, 1, 'Продукция', '/products'),
(8, 1447091606, 1510055001, 1, 0, 'О компании', '/about'),
(9, 1447091617, 1510055129, 1, 4, 'Контакты', '/contacts'),
(11, 1453977094, 1510326028, 1, 3, 'Доставка', '/delivery');

-- --------------------------------------------------------

--
-- Структура таблиці `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) NOT NULL,
  `sort` int(10) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `type` enum('sertificate','partner','offer') DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sliders`
--

INSERT INTO `sliders` (`id`, `sort`, `status`, `type`, `name`, `image`, `pdf`, `link`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'sertificate', 'Ліцензія Державної служби України з надзвичайних ситуацій', '034a5e71f96c460c7dc3bd39662a7380.jpg', 'c6457b22adb8d0115b4b9661a0f13f6c.pdf', NULL, NULL, 1510231956),
(3, 0, 1, 'sertificate', 'Ліцензія Державної архітектурно-будівельної інспекції України', 'ea216cc2ee882f609c853019fa9b29c4.jpg', NULL, NULL, 1510226263, 1510232516),
(4, 0, 1, 'sertificate', 'Ліцензія Державної інспекції ядерного регулювання України', '74280bcdc95782730039a9d2eabb6584.jpg', NULL, NULL, 1510231107, 1510231547),
(5, 0, 1, 'sertificate', 'Ліцензія Державної служби України з надзвичайних ситуацій', 'a78be9a6c44dc335aeccd605e0d31bfb.jpg', NULL, NULL, 1510231568, NULL),
(6, 0, 1, 'sertificate', 'Ліцензія Державної служби України з надзвичайних ситуацій', '24854a5fae08fc0f9df72b6b28cbcf55.jpg', NULL, NULL, 1510231598, NULL),
(7, 0, 1, 'partner', NULL, '222213f2f856bc176022a08a22cf8a82.png', NULL, 'https://www.google.com/', 1510234440, 1510235892),
(8, 0, 1, 'partner', NULL, '0833fba1446076655cead13e54292190.png', NULL, 'https://www.facebook.com/', 1510234600, 1510234991),
(10, 0, 1, 'partner', NULL, '4e2ef413c896007d54f2ee32a8090f98.png', NULL, 'https://twitter.com/', 1510235016, NULL),
(11, 0, 1, 'partner', NULL, '43715525a7d346b85cbc64fe28794c9a.png', NULL, 'http://linkedin.com/', 1510235034, NULL),
(12, 0, 1, 'partner', NULL, '35c7f66ffc9e536d386715f8c3912aa9.jpg', NULL, 'http://apple.com/', 1510235057, NULL),
(13, 0, 1, 'offer', 'Продукты', NULL, NULL, NULL, 1510240875, NULL),
(14, 0, 1, 'offer', 'Услуги', NULL, NULL, NULL, 1510240884, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET cp1251 DEFAULT NULL,
  `login` varchar(128) CHARACTER SET cp1251 DEFAULT NULL,
  `password` varchar(128) CHARACTER SET cp1251 DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `hash` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(2) NOT NULL DEFAULT '1',
  `ip` varchar(16) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `last_login` int(10) DEFAULT NULL,
  `logins` int(10) DEFAULT '0',
  `last_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `name`, `login`, `password`, `created_at`, `updated_at`, `hash`, `email`, `status`, `role_id`, `ip`, `phone`, `last_login`, `logins`, `last_name`, `middle_name`) VALUES
(1, 'Администратор', 'admin', 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', 1418300546, 1430939378, '48e00180ccc77b94d73ec413b015a4cfb9aa58ba10d8c1b63aad5c8317847d9f', 'palenaya.v.wezom111@gmail.com', 1, 4, NULL, '+38 (111) 111-11-11', NULL, 0, NULL, NULL),
(2, 'weZom', 'wezom', '4958070fab7cebd8b1000c6c8cb1bca4aa23b509ee9bc4b70570b5c0e3dfe64a', NULL, 1435164507, 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', 'vitaliy.demyjkane3nko.1991@gmail.com', 1, 3, NULL, '+38 (567) 567-56-75', NULL, 0, NULL, NULL),
(30, 'Dmytro', 'suranov.d.wezom@gmail.com', '2d80aaa210d544a24033633250af946996d142c45839cec1f59015aebb990c1d', 1509976131, 1509976155, NULL, NULL, 1, 5, NULL, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `users_networks`
--

DROP TABLE IF EXISTS `users_networks`;
CREATE TABLE IF NOT EXISTS `users_networks` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `network` varchar(32) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  `profile` varchar(128) NOT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(2) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `alias` varchar(128) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users_roles`
--

INSERT INTO `users_roles` (`id`, `name`, `description`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Пользователь', 'Обычный пользователь, зарегистрировавшийся на сайте', 'user', NULL, NULL),
(3, 'Разработчик', 'Полный доступ ко всему + к тому к чему нет доступа у главного администратора', 'developer', NULL, NULL),
(4, 'Суперадмин', 'Доступ ко всем разделам, кроме тех, к которым имеет доступ только разработчик', 'superadmin', NULL, NULL),
(5, '1', '1', 'admin', 1447421386, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `country` varchar(63) DEFAULT NULL,
  `region` varchar(63) DEFAULT NULL,
  `city` varchar(63) DEFAULT NULL,
  `longitude` varchar(15) DEFAULT NULL,
  `latitude` varchar(15) DEFAULT NULL,
  `answer` text,
  `first_enter` int(10) DEFAULT NULL,
  `last_enter` int(10) DEFAULT NULL,
  `enters` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `visitors_hits`
--

DROP TABLE IF EXISTS `visitors_hits`;
CREATE TABLE IF NOT EXISTS `visitors_hits` (
  `id` int(10) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` varchar(32) NOT NULL DEFAULT '200 OK',
  `device` varchar(32) NOT NULL DEFAULT 'Computer',
  `useragent` varchar(255) DEFAULT NULL,
  `counter` int(10) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `visitors_referers`
--

DROP TABLE IF EXISTS `visitors_referers`;
CREATE TABLE IF NOT EXISTS `visitors_referers` (
  `id` int(10) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `callback`
--
ALTER TABLE `callback`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE,
  ADD KEY `parent_id` (`parent_id`) USING BTREE,
  ADD KEY `cat_brand_alias` (`brand_alias`) USING BTREE,
  ADD KEY `cat_model_alias` (`model_alias`) USING BTREE,
  ADD KEY `image_link` (`image`) USING BTREE;

--
-- Індекси таблиці `catalog_images`
--
ALTER TABLE `catalog_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE,
  ADD KEY `image` (`image`) USING BTREE;

--
-- Індекси таблиці `catalog_tree`
--
ALTER TABLE `catalog_tree`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Індекси таблиці `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `block` (`group`) USING BTREE,
  ADD KEY `type` (`type`) USING BTREE;

--
-- Індекси таблиці `config_groups`
--
ALTER TABLE `config_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Індекси таблиці `config_types`
--
ALTER TABLE `config_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Індекси таблиці `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `action` (`alias`) USING BTREE;

--
-- Індекси таблиці `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `cron`
--
ALTER TABLE `cron`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_id` (`gallery_id`) USING BTREE;

--
-- Індекси таблиці `i18n`
--
ALTER TABLE `i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias_2` (`alias`);

--
-- Індекси таблиці `instructions`
--
ALTER TABLE `instructions`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `mail_templates`
--
ALTER TABLE `mail_templates`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `popup_messages`
--
ALTER TABLE `popup_messages`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `seo_links`
--
ALTER TABLE `seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link` (`link`) USING BTREE;

--
-- Індекси таблиці `seo_redirects`
--
ALTER TABLE `seo_redirects`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `seo_scripts`
--
ALTER TABLE `seo_scripts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place` (`place`) USING BTREE;

--
-- Індекси таблиці `seo_templates`
--
ALTER TABLE `seo_templates`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `sitemap`
--
ALTER TABLE `sitemap`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `sitemenu`
--
ALTER TABLE `sitemenu`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`) USING BTREE,
  ADD UNIQUE KEY `hash` (`hash`) USING BTREE,
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- Індекси таблиці `users_networks`
--
ALTER TABLE `users_networks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Індекси таблиці `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ip` (`ip`) USING BTREE,
  ADD KEY `country` (`country`) USING BTREE;

--
-- Індекси таблиці `visitors_hits`
--
ALTER TABLE `visitors_hits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`) USING BTREE;

--
-- Індекси таблиці `visitors_referers`
--
ALTER TABLE `visitors_referers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`) USING BTREE;

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `callback`
--
ALTER TABLE `callback`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT для таблиці `catalog_images`
--
ALTER TABLE `catalog_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1138;
--
-- AUTO_INCREMENT для таблиці `catalog_tree`
--
ALTER TABLE `catalog_tree`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT для таблиці `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT для таблиці `config_groups`
--
ALTER TABLE `config_groups`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `config_types`
--
ALTER TABLE `config_types`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `content`
--
ALTER TABLE `content`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `control`
--
ALTER TABLE `control`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблиці `cron`
--
ALTER TABLE `cron`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT для таблиці `i18n`
--
ALTER TABLE `i18n`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `instructions`
--
ALTER TABLE `instructions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `log`
--
ALTER TABLE `log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `mail_templates`
--
ALTER TABLE `mail_templates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблиці `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT для таблиці `popup_messages`
--
ALTER TABLE `popup_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблиці `seo_links`
--
ALTER TABLE `seo_links`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `seo_redirects`
--
ALTER TABLE `seo_redirects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `seo_scripts`
--
ALTER TABLE `seo_scripts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `seo_templates`
--
ALTER TABLE `seo_templates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `sitemap`
--
ALTER TABLE `sitemap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблиці `sitemenu`
--
ALTER TABLE `sitemenu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблиці `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблиці `users_networks`
--
ALTER TABLE `users_networks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `visitors_hits`
--
ALTER TABLE `visitors_hits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `visitors_referers`
--
ALTER TABLE `visitors_referers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `catalog`
--
ALTER TABLE `catalog`
  ADD CONSTRAINT `catalog_ibfk_3` FOREIGN KEY (`image`) REFERENCES `catalog_images` (`image`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_ibfk_4` FOREIGN KEY (`parent_id`) REFERENCES `catalog_tree` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `catalog_images`
--
ALTER TABLE `catalog_images`
  ADD CONSTRAINT `catalog_images_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `config`
--
ALTER TABLE `config`
  ADD CONSTRAINT `config_ibfk_1` FOREIGN KEY (`group`) REFERENCES `config_groups` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `config_ibfk_2` FOREIGN KEY (`type`) REFERENCES `config_types` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD CONSTRAINT `gallery_images_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `users_roles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `users_networks`
--
ALTER TABLE `users_networks`
  ADD CONSTRAINT `users_networks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `visitors_hits`
--
ALTER TABLE `visitors_hits`
  ADD CONSTRAINT `visitors_hits_ibfk_1` FOREIGN KEY (`ip`) REFERENCES `visitors` (`ip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `visitors_referers`
--
ALTER TABLE `visitors_referers`
  ADD CONSTRAINT `visitors_referers_ibfk_1` FOREIGN KEY (`ip`) REFERENCES `visitors` (`ip`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
